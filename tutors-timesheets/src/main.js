define(function (require) {
   'use strict';

   var
      _ = require('underscore'),
      $ = require('jquery'),
      Component = require('Component'),
      template = require('/template/main'),
      requester = require('requester'),
      router = require('router'),
      store = require('store');

   return Component.extend({

      template: template,

      events: {
         dom: {
            'click input[type=radio][name=workType]': 'changeDropdownOptions',
            'click [data-add-timesheet]': 'addTimeSheet',
            'click [data-send-timesheet]': 'sendTimeSheet',
            'click [data-delete-timesheet]': 'deleteTimeSheet',
            'click .fa-times': 'removeTimeSheet'
         },
         self: {
            'state:changed': 'render'
         },
         store: 'handleStoreUpdate'
      },
      handleStoreUpdate: function (newState) {
         this.setState(newState);
      },
      changeDropdownOptions: function (e) {
         var html = "";
         if ($(e.currentTarget).attr("id") == "absence") {
            $("input[name=workHours]").attr("disabled", true);
            $("input[name=workHours]").val(0);
            html +=
               "<option>Sjukdom</option>" +
               "<option>Ledighet</option>";
         }
         else {
            $("input[name=workHours]").attr("disabled", false);
            $("input[name=workHours]").val(3);
            html +=
               "<option>Ordinarie läxhjälpspass</option>" +
               "<option>Utbildning</option>" +
               "<option>Kick-off</option>" +
               "<option>Annat överenskommet arbete</option>" +
               "<option>Möte med grupp/skola</option>" +
               "<option>Kontaktansvar</option>";
         }
         $("#workstatus").empty();
         $("#workstatus").append(html);
      },
      addTimeSheet: function () {
         if ($('.lax-alert--success').length) {
            $('.lax-alert--success').remove();
         }
         if ($('.lax-alert--danger').length) {
            $('.lax-alert--danger').remove();
         }
         var inputObject = {};

         $(".lax-form").find(":input").each(function () {
            if (this.type !== "button" && this.type !== "submit") {
               if (this.type === "radio" && $(this).is(":checked")) {
                  inputObject[this.name] = encodeURIComponent($(this).val());
               }
               else if (this.type !== "radio") {
                  inputObject[this.name] = encodeURIComponent($(this).val());
               }
            }
         });
         if (inputObject) {
            if ($('.lax-form__text--success').length) {
               $('.lax-form__text--success').remove();
            }
            var html = '<ul class="lax-list" style="display:none" data-object=' + JSON.stringify(inputObject) + '>';

            $.each(inputObject, function (key) {
               if (key !== "region" && key !== "userid") {
                  html += "<li class='lax-list__item'>" + decodeURIComponent(inputObject[key]) + "</li>";
               }
            });
            html += "<li class='lax-list__item'><i class='fa fa-times fa-lg' aria-hidden='true'></i></li>";
            html += "</ul>";
            $('.lax-form').append(html);
            $('.lax-list').fadeIn("300", function () { });

            if ($('.lax-list').length) {
               if ($('.lax-form__text--date').length) {
                  var hours = $(".lax-form__text--date").data("hours");
                  hours = parseFloat(hours) + parseFloat(inputObject.workHours);
                  $(".lax-form__text--date").data("hours", hours);
                  $(".lax-form__text--date").text('Arbetade timmar: ' + hours + '');

               } else {
                  $('.lax-form').after('<div class="lax-action-group"><p class="lax-form__text"><button type="button" class="lax-button__primary env-button env-button--primary" data-send-timesheet>Skicka in</button></p><p class="lax-form__text lax-form__text--date" data-hours=' + inputObject.workHours + '>Arbetade timmar: ' + inputObject.workHours + ' </p></div>');
               }
            }
         }
      },
      removeTimeSheet: function (e) {
         $(e.currentTarget).closest('.lax-list').fadeOut("300", function () {
            if ($('.lax-list').length) {
               if ($('.lax-list').length === 1) {
                  $('.lax-action-group').remove();
               } else {
                  var hours = 0;
                  $(".lax-list").each(function () {
                     if ($(this)[0] !== $(e.currentTarget).closest('.lax-list')[0]) {
                        var dataObj = JSON.parse($(this).attr('data-object'));
                        hours = hours + parseFloat(dataObj.workHours);
                     }
                  });
                  $(".lax-form__text--date").data("hours", hours);
                  $(".lax-form__text--date").text('Arbetade timmar: ' + hours + '');
               }
            }
            $(this).remove();
         });
      },
      sendTimeSheet: function () {
         var timeSheets = [];

         $(".lax-list").each(function () {
            timeSheets.push($(this).data('object'));
         });
         requester.doPost({
            url: router.getUrl('/add-timesheets'),
            data: {
               timesheets: JSON.stringify(timeSheets)
            },
            context: this
         }).done(function (response) {
            store.dispatch({
               type: 'UPDATED_TIMESHEETS',
               message: response.message,
               timeSheetStatus: response.timeSheetStatus,
               currentDateString: response.currentDateString,
               success: response.success
            });
         });
      },
      deleteTimeSheet: function (e) {
         requester.doDelete({
            url: router.getUrl('/delete-timesheet'),
            data: {
               dsid: $(e.currentTarget).data('dsid')
            },
            context: this
         }).done(function (response) {
            store.dispatch({
               type: 'UPDATED_TIMESHEETS',
               message: response.message,
               timeSheetStatus: response.timeSheetStatus,
               currentDateString: response.currentDateString,
               success: response.success
            });
         });
      },
      filterState: function (state) {
         return _.extend({}, { timeSheetStatus: state.timeSheetStatus, userid: state.userid, message: state.message, currentDateString: state.currentDateString, success: state.success });
      }
   });
});