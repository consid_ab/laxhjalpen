// reducer.js
define(function (require) {
    'use strict';

    var _ = require('underscore');

    var reducer = function (state, action) {
        switch (action.type) {
            case 'UPDATED_TIMESHEETS':
                return _.extend({}, state, { timeSheetStatus: action.timeSheetStatus, message: action.message, currentDateString: action.currentDateString, success: action.success });
            default:
                return state;
        }
    };

    return reducer;
});