(function () {
   'use strict';

   const
      appData = require('appData'),
      router = require('router'),
      logUtil = require('LogUtil'),
      endecUtil = require('EndecUtil'),
      portletContextUtil = require('PortletContextUtil'),
      userUtil = require('UserUtil'),
      storage = require('storage'),
      tutors = storage.getCollectionDataStore('tutors'),
      schools = storage.getCollectionDataStore('schools'),
      timesheets = storage.getCollectionDataStore('timesheets');

   const
      currentUser = portletContextUtil.getCurrentUser(),
      currentUserId = currentUser.getIdentifier(),
      tutorsGroup = appData.getNode('tutorgroup'),
      currentDate = new Date().toISOString(),
      currentDateString = currentDate.split("T")[0];

   function getTutorTimeSheets(currentUserId, timeSheetObject) {
      let findTutors = tutors.find('ds.analyzed.userid:' + currentUserId, 99999);

      try {
         findTutors.toArray((err, data) => {
            if (err) {
               logUtil.info('Err in findTutors toArray - ' + JSON.stringify(err));
            }
            else {
               if (data) {
                  if (data.length === 1) {
                     for (var i = 0; i < data.length; i++) {
                        if (data[i].hasOwnProperty("school")) {
                           timeSheetObject.schoolProperty = true;
                           var regionResult = schools.find('ds.analyzed.name:' + data[i].school, 99999);
                           try {
                              regionResult.toArray((err, data) => {
                                 timeSheetObject.region = data[0].region;
                              });
                           } catch (e) {
                              // Error handling
                              logUtil.info('Error in regionResult toArray - ' + JSON.stringify(e));
                           }
                        }

                        timeSheetObject.dsid = data[i].dsid;
                     }
                     //Get timsheets from user id
                     var findTimesheets = timesheets.find('ds.analyzed.userid:' + currentUserId, 99999);
                     try {
                        findTimesheets.toArray((err, data) => {
                           if (err) {
                              logUtil.info('Err in findTimesheets toArray - ' + JSON.stringify(err));
                           }
                           else {
                              for (var y = 0; y < data.length; y++) {
                                 if (data[y].workState === "Väntande") {
                                    timeSheetObject.waiting.push(data[y]); //lägg till i array
                                 } else if (data[y].workState === "Avslagen") {
                                    timeSheetObject.rejected.push(data[y]);
                                 }

                              }
                           }
                        });
                     } catch (e) {
                        // Error handling
                        logUtil.info('Error in findTimesheets toArray - ' + JSON.stringify(e));
                     }
                  }
               }
            }
         });
      } catch (e) {
         // Error handling
         logUtil.info('Error in findTutors toArray - ' + JSON.stringify(e));
      }
      return timeSheetObject;
   }
   //Init variables
   var
      timeSheetStatusObj = {
         waiting: [], //göras om till array
         rejected: [],
         dsid: null,
         schoolProperty: false,
         region: null
      },
      message = null,
      success = false;

   var memberOfTutors = userUtil.isMemberOfGroup(tutorsGroup);

   if (currentUser) {
      timeSheetStatusObj = getTutorTimeSheets(currentUserId, timeSheetStatusObj);
   }

   router.get('/', function (req, res) {
      if (memberOfTutors && timeSheetStatusObj.schoolProperty) {
         res.render('/', {
            currentDateString: currentDateString,
            timeSheetStatus: timeSheetStatusObj,
            userid: currentUserId,
            message: message,
            success: success
         });
      } else {
         if (memberOfTutors) {
            res.send('Ni saknar koppling till skola.');
         } else {
            res.send('Ni har inte behörighet att se detta material.');
         }
      }
   });
   router.post('/add-timesheets', (req, res) => {

      var
         requestedTimesheets = JSON.parse(req.params.timesheets),
         message = "",
         success = false;
      if (requestedTimesheets.length) {

         for (var y = 0; y < requestedTimesheets.length; y++) {
            requestedTimesheets[y].workDate = new Date(requestedTimesheets[y].workDate).toISOString();
            requestedTimesheets[y].sendDate = currentDate;
            requestedTimesheets[y].workState = "Väntande";
            requestedTimesheets[y].workType = endecUtil.escapeXML(decodeURIComponent(requestedTimesheets[y].workType));
            requestedTimesheets[y].workStatus = endecUtil.escapeXML(decodeURIComponent(requestedTimesheets[y].workStatus));
            try {
               timesheets.add(requestedTimesheets[y], (err, data) => {
                  if (err) {
                     // handle error
                     logUtil.info('Error in areas add - ' + JSON.stringify(err));
                     success = false;
                  } else {
                     logUtil.info('Data is ' + JSON.stringify(data));
                     //Will update the index right on - use reducer to initiate store change.                     
                     try {
                        timesheets.instantIndex(data.dsid);
                        success = true;
                     } catch (e) {
                        // Error handling
                        logUtil.info('Error - in store instant index after timesheets ADD - ' + JSON.stringify(e));
                        success = false;
                     }
                  }
               });

            } catch (e) {
               // Error handling
               logUtil.info('Error in timesheets addAll - ' + JSON.stringify(err));
               success = false;
            }
         }
         if (success) {
            message += "Dina tidrapporter är inskickade!";
         } else {
            message += "Ett fel uppstod när du skickade in dina tidrapporter, var god försök igen.";
         }
      }
      res.json({
         currentDateString: currentDateString,
         timeSheetStatus: getTutorTimeSheets(currentUserId, { waiting: [], rejected: [], dsid: null, hasSchoolProperty: false, region: null }),
         userid: currentUserId,
         message: message,
         success: success
      });
   });
   /*
   Delete single timesheet
   */
   router.delete('/delete-timesheet', (req, res) => {
      var
         dsid = req.params.dsid,
         result = null,
         message = null,
         success = false;

      try {
         result = timesheets.remove(dsid);
         try {
            timesheets.instantIndex(dsid);
            success = true;
            message = "Tidsrapporten togs bort!";
         } catch (e) {
            // Error handling
            logUtil.info('Error in tutors instantindex - ' + JSON.stringify(e));
            success = true;
            message = "Ett fel inträffade vid borttagning av tidsrapport, vänligen försök igen.";
         }
      } catch (e) {
         // Error handling
         logUtil.info('Error in timesheets remove - ' + JSON.stringify(e));
         success = true;
         message = "Ett fel inträffade vid borttagning av tidsrapport, vänligen försök igen.";
      }

      res.json({ message: message, timeSheetStatus: getTutorTimeSheets(currentUserId, { waiting: [], rejected: [], dsid: null, hasSchoolProperty: false }), userid: currentUserId, currentDateString: currentDateString, success: success });
   });
}());