define(function (require) {
    'use strict';

    const
        appData = require('appData'),
        logUtil = require('LogUtil'),
        fileUtil = require('FileUtil'),
        properties = require('Properties'),
        privileged = require('privileged'),
        resourceLocatorUtil = require('ResourceLocatorUtil'),
        storage = require('storage'),
        students = storage.getCollectionDataStore('students');

    const fileNode = appData.getNode('file-folder');

    return {
        postStudent: function (req) {
            const file = req.file('file');
            var
                fileIdentifier = null,
                studentObj = null,
                message = "",
                success = false,
                alertType = null;

            //Check if file can be retrieved.
            if (file) {
                // Execute code in privileged mode
                privileged.doPrivilegedAction(() => {
                    var createdFile = fileUtil.createFileFromTemporary(fileNode, file);
                    if (createdFile) {

                        //Get identifier to store in dataStorage
                        fileIdentifier = createdFile.getIdentifier();

                    } else {
                        logUtil.info('File could not be created');
                        success = true;
                        alertType = "error";
                        message += "Filen kunde inte skapas, försök igen.";
                    }

                });

            } else {
                fileIdentifier = "";
            }

            studentObj = {
                name: req.params.firstname + ' ' + req.params.lastname,
                firstname: req.params.firstname,
                lastname: req.params.lastname,
                socialsecurity: req.params.socialsecurity,
                email: req.params.email,
                phone: req.params.phone,
                startdate: req.params.startdate,
                school: req.params.school,
                grade: req.params.grade,
                caregiver: req.params.caregiver,
                phonecaregiver: req.params.phonecaregiver,
                file: fileIdentifier,
                status: "Aktiv",
                enddate: "",
                tutor: ""
            };

            //Check if same socialsecurity exists otherwise create.
            var searchSocialSecurity = students.find('ds.analyzed.socialsecurity:' + req.params.socialsecurity, 99999);

            try {
                searchSocialSecurity = searchSocialSecurity.toArray();
                if (searchSocialSecurity.length > 0) {
                    success = true;
                    alertType = "error";
                    message += "Personnumret du försöker lägga till finns redan.";
                } else {
                    try {
                        //Add student.
                        students.add(studentObj, (err, data) => {
                            if (err) {
                                // handle error
                                logUtil.info('Error in students add - ' + JSON.stringify(err));
                            } else {
                                try {
                                    students.instantIndex(data.dsid);
                                    success = true;
                                    alertType = "success";
                                    message += "Eleven är tillagd!";
                                } catch (e) {
                                    // Error handling
                                    logUtil.info('Error - in store instant index after student ADD - ' + JSON.stringify(e));
                                    success = true;
                                    alertType = "error";
                                    message += "Eleven kunde inte uppdateras i databasen, försök igen.";
                                }
                            }
                        });
                    } catch (e) {
                        // Error handling
                        logUtil.info('Error caught in students ADD method - ' + JSON.stringify(e));
                        success = true;
                        alertType = "error";
                        message += "Eleven kunde inte läggas till, försök igen.";
                    }
                }
            }
            catch (e) {
                logUtil.info('Error caught in socialSecurity toArray- ' + JSON.stringify(e));
                success = true;
                alertType = "error";
                message += "Eleven kunde inte läggas till, försök igen.";
            }

            return { success: success, alertType: alertType, message: message };
        },
        getStudent: function (queryData) {
            var
                dataObj = JSON.parse(queryData),
                searchStringArray = [],
                searchString = "";

            for (let key in dataObj) {
                if (dataObj.hasOwnProperty(key)) {
                    let objectString = 'ds.analyzed.' + key + ':' + dataObj[key];
                    searchStringArray.push(objectString);
                }
            }
            if (searchStringArray.length) {
                for (var i = 0; i < searchStringArray.length; i++) {
                    if (i == searchStringArray.length - 1) {
                        searchString += searchStringArray[i] + '*';
                    } else {
                        searchString += searchStringArray[i] + '* AND ';
                    }
                }

                //Do search
                var
                    findStudent = students.find(searchString, 99999),
                    result = null;

                try {
                    result = findStudent.toArray();
                    for (var x = 0; x < result.length; x++) {
                        if (result[x].file !== "") {
                            privileged.doPrivilegedAction(() => {
                                let fileNode = resourceLocatorUtil.getNodeByIdentifier(result[x].file);
                                if (fileNode) {
                                    let fileNodeUrl = properties.get(fileNode, 'URL');
                                    result[x].file = fileNodeUrl;
                                }
                            });
                        }
                        result[x].query = queryData;
                    }
                } catch (e) {
                    // Error handling
                    logUtil.info('Error in findStudent toArray ' + JSON.stringify(e));
                }
                return result;
            }
        },
        editStudent: function (req) {

            const file = req.file('file');

            var
                studentObj = {
                    name: req.params.firstname + ' ' + req.params.lastname,
                    firstname: req.params.firstname,
                    lastname: req.params.lastname,
                    socialsecurity: req.params.socialsecurity,
                    email: req.params.email,
                    phone: req.params.phone,
                    startdate: req.params.startdate,
                    school: req.params.school,
                    grade: req.params.grade,
                    caregiver: req.params.caregiver,
                    phonecaregiver: req.params.phonecaregiver,
                    status: req.params.status,
                    enddate: req.params.enddate
                },
                result = null,
                message = "",
                success = false,
                alertType = null;

            if (!file) {
                if (req.params.hasOwnProperty('file') == false) {
                    studentObj.file = "";
                }
            } else {
                //File is defined create it.
                privileged.doPrivilegedAction(() => {
                    var createdFile = fileUtil.createFileFromTemporary(fileNode, file);
                    if (createdFile) {
                        //Get identifier to store in dataStorage                        
                        studentObj.file = createdFile.getIdentifier();
                    }
                });
            }
            try {
                result = students.get(req.params.dsid);
                if (result) {
                    try {
                        students.set(req.params.dsid, studentObj);
                        try {
                            students.instantIndex(req.params.dsid);
                            success = true;
                            alertType = "success";
                            message = "Du uppdaterade " + studentObj.name;
                        } catch (e) {
                            // Error handling
                            logUtil.info('Error - in store instant index after student set - ' + JSON.stringify(e));
                            success = true;
                            alertType = "error";
                            message = studentObj.name + " kunde inte uppdateras i databasen, försök igen.";
                        }
                    } catch (e) {
                        // Error handling
                        logUtil.info('Error in students SET ' + JSON.stringify(e));
                        success = true;
                        alertType = "error";
                        message = "Kunde inte uppdatera eleven i databasen, försök igen.";
                    }
                }
            } catch (e) {
                // Error handling
                logUtil.info('Error in students GET ' + JSON.stringify(e));
                success = true;
                alertType = "error";
                message = "Kunde inte hitta eleven i databasen, försök igen.";
            }
            return { success: success, alertType: alertType, message: message };
        }
    };
});