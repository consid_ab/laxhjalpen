define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        $ = require('jquery'),
        ListComponent = require('ListComponent');

    return ListComponent.extend({

        events: {
            dom: {
                'click a.lax-accordion__heading': 'rotateChevron'
            }
        },
        tagName: 'div',

        className: 'env-list env-list-dividers--bottom',

        childProperty: 'studentList',

        childComponentPath: 'Student',

        attributes: function () {
            return {
                'id': 'lax-accordion'
            };
        },
        rotateChevron: function (e) {
            if ($('.env-alert').length) {
                $('.env-alert').remove();
            }
            if ($('.fa-chevron-up').length) {
                if ($('.fa-chevron-up')[0] != $(e.currentTarget).find('i')[0]) {
                    $('.fa-chevron-up').toggleClass('fa-chevron-up fa-chevron-down');
                }
            }
            $(e.currentTarget).find('i').toggleClass('fa-chevron-down fa-chevron-up');

        },
        filterState: function (state) {
            return _.extend({}, { studentList: state.studentList, schoolsarea: state.schoolsArea, schools: state.schools });
        }
    });
});