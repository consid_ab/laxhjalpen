define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        $ = require('jquery'),
        Component = require('Component'),
        router = require('router'),
        requester = require('requester'),
        store = require('store'),
        template = require('/template/search-students-content');

    return Component.extend({

        template: template,
        events: {
            dom: {
                'submit .lax-form__search-students': 'handleSearch'
            },
            router: {
                'query:changed': 'getStudents'
            },
            self: {
                'state:changed': 'render'
            },
            store: 'handleStoreChange'
        },
        onRendered: function () {
            $('#select-school').select2({
                width: 'resolve',
                language: {
                    noResults: function () {
                        return "Hittade inga skolor.";
                    }
                }
            });
        },
        handleStoreChange: function (newState) {
            this.setState(newState);
        },
        handleSearch: function (e) {
            e.preventDefault();
            var
                queryString = "",
                obj = {};

            $(".lax-form__search-students :input").each(function (i) {
                var keyName;

                if ($(this)[0].tagName === "SELECT") {
                    if ($('option:selected', this).val() !== "") {
                        keyName = $(this)[0].name;
                        obj[keyName] = $('option:selected', this).val();
                    } else {
                        keyName = $(this)[0].name;
                        obj[keyName] = "*";
                    }
                } else {
                    if ($(this)[0].tagName !== "BUTTON") {
                        if ($(this).val() !== "") {
                            keyName = $(this)[i].name;
                            obj[keyName] = $(this).val();
                        }
                        else {
                            keyName = $(this)[0].name;
                            obj[keyName] = "*";
                        }
                    }
                }
                i++;
            });
            queryString += Object.keys(obj).map((key) => { return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]); }).join('&');

            //Route the form action.
            router.navigate(e.currentTarget.action, {
                queryParams: { query: queryString }
            });
        },
        getStudents: function (options) {
            if ($('.env-alert').length) {
                $('.env-alert').remove();
            }
            var queryObject = Object.fromEntries(new URLSearchParams(options.queryParams.query));
            requester.doGet({
                url: router.getUrl('/fetch-student'),
                data: {
                    data: JSON.stringify(queryObject)
                },
                context: this
            }).done(function (response) {
                store.dispatch({
                    type: 'SET_STUDENTS',
                    studentList: response.studentList,
                    editStudent: response.editStudent,
                    query: JSON.parse(response.query)
                });
            });
        },
        filterState: function (state) {
            return _.extend({}, { studentList: state.studentList, schoolsArea: state.schoolsArea, schools: state.schools, query: state.query, editStudent: state.editStudent });
        }
    });
});