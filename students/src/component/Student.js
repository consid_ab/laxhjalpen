define(function (require) {
    'use strict';

    var
        Component = require('Component'),
        studentTemplate = require('/template/student');

    return Component.extend({

        template: studentTemplate,

        tagName: 'div',

        className: 'lax-accordion__wrapper env-list__item'

    });
});