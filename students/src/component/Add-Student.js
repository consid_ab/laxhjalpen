define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        $ = require('jquery'),
        Component = require('Component'),
        template = require('/template/add-student');

    return Component.extend({

        template: template,
        events: {
            dom: {
                'change input[type="file"]': 'validateFileType',
                'submit .lax-form__add-student': 'disableButton'
            },
        },
        validateFileType: function (e) {
            const
                fileName = e.target.value,
                lastIndexOfDot = fileName.lastIndexOf(".") + 1,
                extensionFile = fileName.substr(lastIndexOfDot, fileName.length).toLowerCase();

            if (["jpg", "jpeg", "png", "tif", "gif", "pdf"].includes(extensionFile)) {
                if ($('.env-alert').length) {
                    $('.env-alert').remove();
                }
                return true;
            } else {
                var html = '<div class="lax-form-alert env-alert env-alert--danger" role="alert"><strong>Endast bild eller PDF är tillåtet.</strong></div>';
                $('.lax-panel').append(html);
                e.target.value = "";
            }
        },
        disableButton: function (e) {
            $(e.currentTarget).find('.env-button').attr("disabled", true);
        },
        filterState: function (state) {
            return _.extend({}, { schools: state.schools, schoolsArea: state.schoolsArea, currentDateString: state.currentDateString, success: state.success, alertType: state.alertType, message: state.message });
        }
    });
});