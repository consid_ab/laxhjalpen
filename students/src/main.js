define(function (require) {
   'use strict';

   var
      _ = require('underscore'),
      $ = require('jquery'),
      Component = require('Component'),
      template = require('/template/main'),
      searchTemplate = require('/template/search-students');

   return Component.extend({

      getTemplate: function () {
         if (this.state.route === '/search-students') {
            return searchTemplate;
         } else {
            return template;
         }
      },
      onRendered: function () {
         $('#select-school').select2({
            placeholder: "Välj eller sök skola",
            width: 'resolve',
            language: {
               noResults: function () {
                  return "Hittade inga skolor.";
               }
            }
         });
      },
      filterState: function (state) {
         return _.extend({}, { studentList: state.studentList, schools: state.schools, schoolsArea: state.schoolsArea, currentDateString: state.currentDateString, success: state.success, alertType: state.alertType, message: state.message, query: state.query, editStudent: state.editStudent });
      }
   });
});