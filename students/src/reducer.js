
define(function (require) {
    'use strict';

    var _ = require('underscore');

    var reducer = function (state, action) {
        switch (action.type) {
            case 'SET_STUDENTS':
                return _.extend({}, state, { studentList: action.studentList, editStudent: action.editStudent, query: action.query });
            default:
                return state;
        }
    }

    return reducer;
});