(function () {
   'use strict';

   const
      logUtil = require('LogUtil'),
      studentStorageHandler = require('/module/server/studentStorageHandler'),
      router = require('router'),
      storage = require('storage'),
      schools = storage.getCollectionDataStore('schools');

   const
      currentDate = new Date().toISOString(),
      currentDateString = currentDate.split("T")[0];

   /*
     Get all items in a datastorage and return in alphabetical order.
   */
   function getAllfromStorage(dataStorage) {
      var
         result = dataStorage.find('*', 99999),
         listOfItems = null;
      try {
         listOfItems = result.toArray().sort(function (a, b) {
            a = a.name.toLowerCase();
            b = b.name.toLowerCase();
            if (a == b) return 0;
            return a < b ? -1 : 1;
         });
      } catch (e) {
         logUtil.info('Error when trying to convert items to array in getAllFromStorage - ' + JSON.stringify(e));
      }
      return listOfItems;
   }
   /*
     Get all unique school locations.
     Sort alphabetically
   */
   function getSchoolAreas(list) {
      var schoolsArea = [];
      for (var i = 0; i < list.length; i++) {
         if (schoolsArea.indexOf(list[i].area) === -1) {
            schoolsArea.push(list[i].area);
         }
      }
      return schoolsArea.sort();
   }
   /* 
      Views
   */
   router.get('/', function (req, res) {

      var
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools);

      res.render('/', { studentList: null, query: "", schools: availableSchools, schoolsArea: schoolsArea, currentDateString: currentDateString, success: false, alertType: null, message: "", editStudent: null });
   });

   router.get('/search-students', function (req, res) {
      var
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools),
         studentList = null,
         query = null;

      if (req.params.hasOwnProperty('query')) {
         var queryString = req.params.query;
         var queryObject = queryString.split("&").reduce(function (prev, curr) {
            let p = curr.split("=");
            prev[p[0]] = decodeURIComponent(p[1]);
            return prev;
         }, {});
         studentList = studentStorageHandler.getStudent(JSON.stringify(queryObject));
         query = queryObject;
      }

      res.render('/search-students', { studentList: studentList, schools: availableSchools, schoolsArea: schoolsArea, query: query, editStudent: null });
   });
   router.get('/edit-student', function (req, res) {
      //Should never be here.
      var
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools),
         studentList = null,
         query = null;

      res.render('/search-students', { studentList: studentList, schools: availableSchools, schoolsArea: schoolsArea, query: query, editStudent: null });
   });
   /* 
      Methods 
   */
   router.post('/add-student', function (req, res) {

      var
         addStudent = studentStorageHandler.postStudent(req),
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools);

      res.render('/', { studentList: null, query: "", schools: availableSchools, schoolsArea: schoolsArea, currentDateString: currentDateString, success: addStudent.success, alertType: addStudent.alertType, message: addStudent.message });
   });
   /*
   Fetch student from search form. 
   */
   router.get('/fetch-student', function (req, res) {

      var
         studentList = studentStorageHandler.getStudent(req.params.data),
         query = req.params.data,
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools);

      res.json({ studentList: studentList.sort((a, b) => a.name.localeCompare(b.name)), query: query, schoolsArea: schoolsArea, editStudent: null });
   });
   router.post('/edit-student', function (req, res) {
      var
         editStudent = studentStorageHandler.editStudent(req),
         studentList = studentStorageHandler.getStudent(req.params.query),
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools);

      res.render('/search-students', { editStudent: editStudent, studentList: studentList.sort((a, b) => a.name.localeCompare(b.name)), query: JSON.parse(req.params.query), schools: availableSchools, schoolsArea: schoolsArea });
   });
}());