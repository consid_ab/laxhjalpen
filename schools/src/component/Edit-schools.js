define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        Component = require('Component'),
        $ = require('jquery'),
        store = require('store'),
        router = require('router'),
        requester = require('requester'),
        template = require('/template/edit-schools-content');

    return Component.extend({

        template: template,

        events: {
            dom: {
                'change select#select-school': 'findSchool',
                'submit .lax-form__edit-school-information': 'putSchool',
                'click button[data-modal-dialog-dismiss]': 'useModal',
                'click i.lax-icon__close': 'useModal'
            },
            self: {
                'state:changed': 'render'
            },
            store: 'handleStoreUpdate'
        },
        handleStoreUpdate: function (newState) {
            this.setState(newState);
        },
        onRendered: function () {
            $('#select-school').select2({
                placeholder: "Välj eller sök skola",
                width: 'resolve'
            });
        },
        useModal: function () {
            $('.lax-form__edit-school-information').toggleClass(function () {
                $('.lax-form__edit-school-information').removeClass('env-modal-dialog--show');
                $('.lax-modal-wrapper').remove();
            });

        },
        findSchool: function (e) {
            e.preventDefault();

            if ($('.lax-alert').length) {
                $('.lax-alert').remove();
            }
            var html = "";

            requester.doGet({
                url: router.getUrl('/get-school'),
                data: {
                    dsid: $(e.currentTarget).find('option:selected').data('dsid')
                },
                context: this
            }).done(function (response) {
                if (response) {
                    html +=
                        '<div class="lax-modal-wrapper">' +
                        '<div id="edit-school-form" class="env-modal-dialog env-modal-dialog--show" role="dialog" aria-labelledby="myDialog" aria-hidden="true" tabindex="-1" style="background-color:rgba(0,0,0,.5);">' +
                        '<div class="env-modal-dialog__dialog">' +
                        '<section class="env-modal-dialog__content">' +
                        '<header class="env-modal-dialog__header lax-modal-dialog__header">' +
                        '<h6 class="env-text env-modal-dialog__header__title">Ändra skola</h6>' +
                        '<i class="fa fa-times fa-lg lax-icon__close" aria-hidden="true"></i>' +
                        '</header>' +
                        '<form class="lax-form env-form lax-form__edit-school-information">' +
                        '<div class="env-form">' +
                        '<div class="env-modal-dialog__body">' +
                        '<div class="lax-modal-body__content">' +
                        '<div class="env-form-element__control">' +
                        '<label class="env-form-element__label" for="school">Namn</label>' +
                        '<input class="env-form-input" type="text" id="school-text" name="school" data-dsid="' + response.schools.dsid + '" value="' + response.schools.name + '" readonly required title="Namn går inte att redigera">' +
                        '</div>' +
                        '</div>' +
                        '<div class="lax-modal-body__content">' +
                        '<div class="env-form-element__control">' +
                        '<label class="env-form-element__label" for="adress">Adress</label>' +
                        '<input class="env-form-input" type="text" id="adress-text" name="adress" value="' + response.schools.adress + '" required>' +
                        '</div>' +
                        '</div>' +
                        '<div class="lax-modal-body__content">' +
                        '<div class="env-form-element__control">' +
                        '<label for="area" class="env-form-element__label">Område</label>';
                    let areasIndexPos = response.areas.map(function (e) { return e.dsid; }).indexOf(response.schools.areaId);

                    if (areasIndexPos > -1) {
                        html += '<input class="env-form-input lax-form__select-area" id="select-area" name="area" data-dsid=' + response.schools.areaId + ' required readonly title="Område går inte att redigera" value="' + response.schools.area + '"/>';
                    }

                    html +=
                        '</div>' +
                        '</div>' +
                        '<div class="lax-modal-body__content">' +
                        '<div class="env-form-element__control">' +
                        '<label class="env-form-element__label" for="contact-person">Kontaktperson</label>' +
                        '<input class="env-form-input" type="text" id="contact-person-text" name="contact-person" value="' + response.schools.contactPerson + '" required>' +
                        '</div>' +
                        '</div>' +
                        '<div class="lax-modal-body__content">' +
                        '<div class="env-form-element__control">' +
                        '<label class="env-form-element__label" for="contact-person">Telefonnummer till kontaktperson</label>' +
                        '<input class="env-form-input" type="text" id="contact-person-phone-text" name="contact-person-phone" value="' + response.schools.contactPersonPhone + '" required>' +
                        '</div>' +
                        '</div>' +
                        '<div class="lax-modal-body__content">' +
                        '<div class="env-form-element__control">' +
                        '<label for="select-area" class="env-form-element__label">Läxhjälpare</label>' +
                        '<select class="env-form-input" id="tutors-select-multiple" class="env-form-input" name="tutors[]" multiple="multiple" required>';
                    //Tutors in use
                    for (var x = 0; x < response.schools.tutors.length; x++) {
                        html += '<option data-user-id="' + response.schools.tutors[x].id + '" selected>' + response.schools.tutors[x].name + '</option>';
                    }
                    //Tutors not in use
                    for (var y = 0; y < response.tutors.length; y++) {
                        html += '<option data-user-id="' + response.tutors[y].id + '"> ' + response.tutors[y].name + '</option > ';
                    }

                    html +=
                        '</select>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<footer class="env-modal-dialog__footer lax-modal-dialog__footer">' +
                        '<button type="submit" class="lax-button env-button env-button--primary">Spara</button>' +
                        '<button type="button" data-modal-dialog-dismiss class="env-button">Avbryt</button>' +
                        '</footer>' +
                        '</div>' +
                        '</div>' +
                        '</section>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                    $(html).insertAfter($('.lax-form__edit-school'));
                    $('#tutors-select-multiple').select2({
                        placeholder: "Välj läxhjälpare",
                        closeOnSelect: false,
                        width: 'resolve'
                    });

                }
            }).fail(function (response) {
                console.log('response not ok! - ' + JSON.stringify(response));
            });
        },
        putSchool: function (e) {
            e.preventDefault();
            $(e.currentTarget).find('.env-button').attr("disabled", true);

            var
                tutorsArray = $('#tutors-select-multiple').select2('data'),
                tutors = [];

            for (var i = 0; i < tutorsArray.length; i++) {
                tutors.push({
                    name: tutorsArray[i].id,
                    id: tutorsArray[i].element.dataset.userId
                });
            }
            requester.doPut({
                url: router.getUrl('/put-school'),
                data: {
                    dsid: this.$('input[name=school]').data('dsid'),
                    name: this.$('input[name=school]').val(),
                    adress: this.$('input[name=adress]').val(),
                    area: this.$('input[name=area]').val(),
                    areaId: this.$('input[name=area]').data('dsid'),
                    contactPerson: this.$('input[name=contact-person]').val(),
                    contactPersonPhone: this.$('input[name=contact-person-phone]').val(),
                    tutors: tutors
                },
                context: this
            }).done(function (response) {
                store.dispatch({
                    type: 'EDITED_SCHOOL',
                    schools: response.schools,
                    schoolsArea: response.schoolsArea,
                    responseSuccess: response.responseSuccess,
                    responseMessage: response.responseMessage,
                    alertType: response.alertType
                });
            }).fail(function (response) {
                //ADD ERROR HANDLING
                console.log(JSON.stringify(response));
            });
        },
        filterState: function (state) {
            return _.extend({}, { schools: state.schools, schoolsArea: state.schoolsArea, responseSuccess: state.responseSuccess, responseMessage: state.responseMessage, alertType: state.alertType });
        }
    });
});