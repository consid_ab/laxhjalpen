define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        Component = require('Component'),
        $ = require('jquery'),
        router = require('router'),
        requester = require('requester'),
        store = require('store'),
        template = require('/template/add-schools');

    return Component.extend({

        template: template,

        events: {
            dom: {
                'submit .lax-form__add-school': 'addSchool'
            }, 
            self: {
                'state:changed': 'render'
            },
            store: 'handleStoreUpdate'
        },
        handleStoreUpdate: function (newState) {
            this.setState(newState);
        },
        onRendered: function () {
            $('#tutors-select-multiple').select2({
                placeholder: "Välj läxhjälpare",
                closeOnSelect: false,
                width: 'resolve',
                language: {
                    noResults: function () {
                        return "Inga läxhjälpare tillgängliga.";
                    }
                },
            });
        },
        addSchool: function (e) {
            e.preventDefault();

            $(e.currentTarget).find('.env-button').attr("disabled", true);

            if ($('.env-alert').length) {
                $('.env-alert').remove();
            }
            var html = "",
                tutorsArray = $('#tutors-select-multiple').select2('data'),
                tutors = [];

            for (var i = 0; i < tutorsArray.length; i++) {
                tutors.push({
                    name: tutorsArray[i].id,
                    id: tutorsArray[i].element.dataset.userId
                });
            }
            requester.doPost({
                url: router.getUrl('/post-school'),
                data: {
                    name: this.$('input[name=school]').val(),
                    adress: this.$('input[name=adress]').val(),
                    area: this.$('select[name=area] option:selected').text(),
                    areaId: this.$('select[name=area] option:selected').data('dsid'),
                    tutors: tutors,
                    contactPerson: this.$('input[name=contact-person]').val(),
                    contactPersonPhone: this.$('input[name=contact-person-phone]').val()
                },
                context: this
            }).done(function (response) {
                store.dispatch({
                    type: 'ADDED_SCHOOL',
                    areas: response.areas,
                    tutors: response.tutors,
                    responseSuccess: response.responseSuccess,
                    responseMessage: response.responseMessage,
                    alertType: response.alertType
                });
            }).fail(function (response) {
                html += '<div class="env-alert lax-alert env-alert--danger" role="alert"><strong>' + response.message + '</strong></div>';
                $('.lax-form').append(html);
                $(e.currentTarget).find('.env-button').attr("disabled", false);
            });
        },
        filterState: function (state) {
            return _.extend({}, { areas: state.areas, tutors: state.tutors, responseSuccess: state.responseSuccess, responseMessage: state.responseMessage, alertType: state.alertType });
        }
    });
});