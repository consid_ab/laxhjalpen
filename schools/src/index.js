(function () {
   'use strict';

   var
      appData = require('appData'),
      router = require('router'),
      logUtil = require('LogUtil'),
      properties = require('Properties'),
      userUtil = require('UserUtil'),
      storage = require('storage'),
      areas = storage.getCollectionDataStore('areas'),
      schools = storage.getCollectionDataStore('schools'),
      schoolStorageHandler = require('/module/server/schoolStorageHandler'),
      tutorStorageHandler = require('/module/server/tutorStorageHandler');

   const
      adminGroup = appData.getNode('usergroup'),
      tutorsGroup = appData.getNode('tutorsgroup');

   /* 
   Functions - re used.
   */
   function iterateUsers(idArray) {

      var users = [];
      if (idArray) {
         if (idArray.length) {
            for (var i = 0; i < idArray.length; i++) {
               users.push({
                  name: properties.get(idArray[i], 'displayName'),
                  id: idArray[i]
               });
            }
            users.sort((a, b) => a.name.localeCompare(b.name));
         }
      }
      return users;
   }
   /*
   Get all items in a datastorage and return in alphabetical order.
   */
   function getAllfromStorage(dataStorage) {
      var
         result = dataStorage.find('*', 99999),
         listOfItems = null;
      try {
         listOfItems = result.toArray().sort(function (a, b) {
            a = a.name.toLowerCase();
            b = b.name.toLowerCase();
            if (a == b) return 0;
            return a < b ? -1 : 1;
         });
      } catch (e) {
         logUtil.info('Rendering error when trying to convert items to array - ' + JSON.stringify(e));
      }
      return listOfItems;
   }
   function getSchoolAreas(list) {
      var schoolsArea = [];
      for (var i = 0; i < list.length; i++) {
         if (schoolsArea.indexOf(list[i].area) === -1) {
            schoolsArea.push(list[i].area);
         }
      }
      return schoolsArea.sort();
   }
   /*
   Administrators - group node selected from config 
   */
   var
      adminIds = properties.get(adminGroup, 'member'),
      currentUserAccess = userUtil.isMemberOfGroup(adminGroup),
      tutorIds = properties.get(tutorsGroup, 'member'),
      tutors = null;

   if (tutorIds) {
      tutors = tutorStorageHandler.checkTutorAvailability(iterateUsers(tutorIds));
   }
   /*
   Views
   */
   router.get('/', function (req, res) {
      if (currentUserAccess && tutors) {
         res.render('/', { areas: getAllfromStorage(areas), tutors: tutors, responseSuccess: false, responseMessage: "", alertType: null, schoolsArea: null });
      }
      else {
         if (adminIds) {
            res.send('Ni har inte behörighet att se detta material.');
         } else {
            res.send('Fel grupp utpekad i konfigurationen.');
         }
      }
   });
   router.get('/edit-schools', function (req, res) {
      if (currentUserAccess) {
         var
            schoolsList = getAllfromStorage(schools),
            schoolsArea = getSchoolAreas(schoolsList);

         res.render('/edit-schools', { schools: schoolsList, schoolsArea: schoolsArea });
      }
   });

   /*
   Methods
   */
   router.post('/post-school', function (req, res) {
      var
         postSchool = schoolStorageHandler.postSchool(req);

      res.json({ areas: getAllfromStorage(areas), tutors: tutorStorageHandler.checkTutorAvailability(iterateUsers(tutorIds)), responseMessage: postSchool.message, responseSuccess: postSchool.success, alertType: postSchool.alertType });
   });

   router.get('/get-school', function (req, res) {
      var getSchool = schoolStorageHandler.fetchSchool(req);

      res.json({ schools: getSchool, areas: getAllfromStorage(areas), tutors: tutors });
   });

   router.put('/put-school', function (req, res) {
      var
         updateSchool = schoolStorageHandler.putSchool(req),
         schoolsList = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(schoolsList);

      res.json({ schools: schoolsList, schoolsArea: schoolsArea, responseSuccess: updateSchool.success, responseMessage: updateSchool.message, alertType: updateSchool.alertType, areas: getAllfromStorage(areas), tutors: tutors });
   });

}());