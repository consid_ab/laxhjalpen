// reducer.js
define(function (require) {
    'use strict';

    var _ = require('underscore');

    var reducer = function (state, action) {
        switch (action.type) {
            case 'ADDED_SCHOOL':
                return _.extend({}, state, { areas: action.areas, tutors: action.tutors, responseSuccess: action.responseSuccess, responseMessage: action.responseMessage, alertType: action.alertType });
            case 'EDITED_SCHOOL':
                return _.extend({}, state, { schools: action.schools, schoolsArea: action.schoolsArea, responseSuccess: action.responseSuccess, responseMessage: action.responseMessage, alertType: action.alertType });
            default:
                return state;
        }
    };

    return reducer;
});