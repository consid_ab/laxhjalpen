define(function (require) {
   'use strict';

   var
      _ = require('underscore'),
      Component = require('Component'),
      template = require('/template/main'),
      editSchoolsTemplate = require('/template/edit-schools');

   return Component.extend({
      getTemplate: function () {
         if (this.state.route === '/edit-schools') {
            return editSchoolsTemplate;
         } else {
            return template;
         }
      },     
      filterState: function (state) {
         return _.extend({}, { schools: state.schools, schoolsArea: state.schoolsArea, areas: state.areas, tutors: state.tutors, responseSuccess: state.responseSuccess, responseMessage: state.responseMessage, alertType: state.alertType });
      }
   });
});