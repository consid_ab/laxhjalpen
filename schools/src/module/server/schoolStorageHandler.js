define(function (require) {
    'use strict';

    var
        endecUtil = require('EndecUtil'),
        logUtil = require('LogUtil'),
        storage = require('storage'),
        schools = storage.getCollectionDataStore('schools'),
        areas = storage.getCollectionDataStore('areas'),
        tutorStorageHandler = require('/module/server/tutorStorageHandler');

    return {

        postSchool: function (req) {

            //Returns status on the added tutors - if true we proceed to add the school.
            var addTutorStatus = tutorStorageHandler.addTutor(req.params.tutors, req.params.name);

            var
                message = "",
                success = false,
                alertType = null;

            if (addTutorStatus) {
                var findSchool = schools.find(req.params.name, 99999);

                try {
                    findSchool.toArray((err, data) => {
                        if (data) {
                            if (data.length === 0) {
                                var area = null;
                                try {
                                    area = areas.get(req.params.areaId);
                                    if (area) {
                                        var schoolObj = {
                                            name: endecUtil.escapeXML(req.params.name),
                                            adress: endecUtil.escapeXML(req.params.adress),
                                            area: endecUtil.escapeXML(req.params.area),
                                            areaId: endecUtil.escapeXML(req.params.areaId),
                                            region: area.region,
                                            tutors: req.params.tutors,
                                            contactPerson: endecUtil.escapeXML(req.params.contactPerson),
                                            contactPersonPhone: endecUtil.escapeXML(req.params.contactPersonPhone)
                                        };

                                        try {
                                            schools.add(schoolObj, (err, data) => {
                                                if (err) {
                                                    // handle error
                                                    logUtil.info('Error in schools add - ' + JSON.stringify(err));
                                                    message += 'Ett fel inträffade';
                                                    success = true;
                                                    alertType = "error";
                                                } else {
                                                    //Will update the index right on - use reducer to initiate store change.                     
                                                    try {
                                                        schools.instantIndex(data.dsid);
                                                        message += "Din skola är tillagd!";
                                                        success = true;
                                                        alertType = "success";
                                                    } catch (e) {
                                                        // Error handling
                                                        logUtil.info('Error - in store instant index after schools ADD - ' + JSON.stringify(e));
                                                        message += 'Ett fel inträffade';
                                                        success = true;
                                                        alertType = "error";
                                                    }
                                                }
                                            });
                                        } catch (e) {
                                            // Error handling
                                            logUtil.info('Error caught in schools ADD method - ' + JSON.stringify(e));
                                            message += 'Ett fel inträffade';
                                            success = true;
                                            alertType = "error";
                                        }
                                    }
                                } catch (e) {
                                    // Error handling
                                    logUtil.info('Error caught in areas get method - ' + JSON.stringify(e));
                                    message += 'Ett fel inträffade';
                                    success = true;
                                    alertType = "error";
                                }
                            } else {
                                message += "Skolan du försöker lägga till finns redan.";
                                success = true;
                                alertType = "error";
                            }

                        }
                    });
                } catch (e) {
                    // Error handling
                    logUtil.info('Error in findArea toArray - ' + JSON.stringify(e));
                    message += 'Ett fel inträffade';
                    success = true;
                    alertType = "error";
                }

                return {
                    message: message,
                    success: success,
                    alertType: alertType
                };
            }
            else {
                return {
                    message: "Det gick inte lägga till användaren/användarna. Titta så att användaren/användarna inte är kopplade till en annan skola",
                    success: true,
                    alertType: "error"
                };
            }
        },
        fetchSchool: function (req) {
            var schoolResult = null;
            try {
                schoolResult = schools.get(req.params.dsid);
            } catch (e) {
                // Error handling
                logUtil.info('Error in getting DSID from school - ' + JSON.stringify(e));
            }
            return schoolResult;
        },
        putSchool: function (req) {
            var
                message = "",
                success = false,
                alertType = null,
                dsid = req.params.dsid,
                schoolsData = {
                    dsid: req.params.dsid,
                    name: req.params.name,
                    adress: req.params.adress,
                    area: req.params.area,
                    areaId: req.params.areaId,
                    contactPerson: req.params.contactPerson,
                    contactPersonPhone: req.params.contactPersonPhone,
                    tutors: req.params.tutors
                };
            /*
            I dagsläget kan du döpa en skola till samma sak när du uppdaterar.
            Checkar kan byggas här med hjälp av att först söka upp skolans namn, finns den, returnera fel, annars hämta posten med hjälp av req.params.dsid
            */
            var searchSchool = schools.find(req.params.name, 99999);

            try {

                let searchResult = searchSchool.toArray();

                var
                    findSchool = schools.get(req.params.dsid),
                    updateTutor = true,
                    addTutor = null;

                if (findSchool) {

                    var currentTutors = findSchool.tutors;

                    if (dsid === findSchool.dsid && schoolsData.name !== findSchool.name) {
                        //Update the tutors                                 
                        for (var i = 0; i < currentTutors.length; i++) {
                            //Check if currentTutor is in the tutorArray we are sending.                                    
                            let currentTutorPos = schoolsData.tutors.map(function (e) { return e.id; }).indexOf(currentTutors[i].id);

                            if (currentTutorPos === -1) {
                                updateTutor = tutorStorageHandler.updateTutors(currentTutors[i].id, findSchool.name);
                            }
                        }

                        addTutor = tutorStorageHandler.addTutor(schoolsData.tutors, schoolsData.name);

                        //Har vi uppdaterat tutor eller fanns de ingen att uppdatera
                        if (updateTutor && addTutor) {
                            try {
                                schools.set(dsid, schoolsData);
                                try {
                                    schools.instantIndex(dsid);
                                    message += "Skolan har uppdaterats.";
                                    success = true;
                                    alertType = "success";
                                } catch (e) {
                                    // Error handling
                                    message += "Ett fel inträffade, försök igen.";
                                    success = true;
                                    alertType = "error";
                                }
                            } catch (e) {
                                // Error handling
                                logUtil.info('Error in regions set - ' + JSON.stringify(e));
                                message += "Ett fel inträffade, försök igen.";
                                success = true;
                                alertType = "error";
                            }
                        }
                    }
                    else {
                        if (dsid === findSchool.dsid && schoolsData.name === findSchool.name) {
                            for (var y = 0; y < currentTutors.length; y++) {
                                let currentTutorPos = schoolsData.tutors.map(function (e) { return e.id; }).indexOf(currentTutors[y].id);

                                if (currentTutorPos === -1) {
                                    //Will remove school from the current iterating tutor
                                    updateTutor = tutorStorageHandler.updateTutors(currentTutors[y].id, findSchool.name);
                                }
                            }
                            addTutor = tutorStorageHandler.addTutor(schoolsData.tutors, schoolsData.name);
                            //Har vi uppdaterat tutor eller fanns de ingen att uppdatera
                            if (updateTutor && addTutor) {
                                //Uppdatera skola
                                try {
                                    schools.set(dsid, schoolsData);
                                    try {
                                        schools.instantIndex(dsid);
                                        message += "Skolan har uppdaterats.";
                                        success = true;
                                        alertType = "success";
                                    } catch (e) {
                                        // Error handling
                                        message += "Ett fel inträffade, försök igen.";
                                        success = true;
                                        alertType = "error";
                                    }
                                } catch (e) {
                                    // Error handling
                                    logUtil.info('Error in schools set - ' + JSON.stringify(e));
                                    message += "Ett fel inträffade, försök igen.";
                                    success = true;
                                    alertType = "error";
                                }
                            } else {
                                //Skicka meddelande
                                logUtil.info('Tutor gick inte att uppdatera eller något fel hände och vi hamnar här.');
                                message += "Ett fel inträffade, försök igen.";
                                success = true;
                                alertType = "error";
                            }
                        } else {
                            // Error handling                               
                            message += "Ett fel inträffade, namnet finns redan.";
                            success = true;
                            alertType = "error";
                        }
                    }
                } else {
                    logUtil.info('skolan finns ej!');
                }
                //}
            } catch (e) {
                logUtil.info('Ett fel inträffade i searchSchool.toArray() - ' + JSON.stringify(e));
            }

            return { message: message, success: success, alertType: alertType };
        }
    };
});