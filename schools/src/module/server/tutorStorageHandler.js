define(function (require) {
    'use strict';

    var
        logUtil = require('LogUtil'),
        storage = require('storage'),
        tutors = storage.getCollectionDataStore('tutors'),
        groups = storage.getCollectionDataStore('groups'),
        students = storage.getCollectionDataStore('students');

    return {
        checkTutorAvailability: function (tutorArray) {

            var tutorResult = [];

            for (var i = 0; i < tutorArray.length; i++) {

                let findTutor = tutors.find('ds.analyzed.userid:' + tutorArray[i].id, 99999);

                try {
                    findTutor.toArray((err, data) => {
                        if (data.length === 0) {
                            tutorResult.push({
                                name: tutorArray[i].name,
                                id: tutorArray[i].id
                            });
                        } else {
                            //We have a user - check if school is empty.
                            if (data[0].school === "") {
                                tutorResult.push({
                                    name: tutorArray[i].name,
                                    id: tutorArray[i].id
                                });
                            }
                        }
                    });
                } catch (error) {
                    // Error handling
                    logUtil.info('checkTutorAvailability  - Caught error in toArray - ' + JSON.stringify(error));
                }

            }
            return tutorResult;
        },
        addTutor: function (tutorArray, schoolName) {

            var status = false;
            for (var i = 0; i < tutorArray.length; i++) {

                let tutorObj = {
                    name: tutorArray[i].name,
                    userid: tutorArray[i].id,
                    school: schoolName,
                    students: []
                };
                var findTutor = tutors.find('ds.analyzed.userid:' + tutorArray[i].id, 99999);

                try {
                    findTutor.toArray((err, data) => {
                        if (err) {
                            logUtil.info('Error in findTutor to array - ' + JSON.stringify(err));
                        }
                        if (data.length > 0) {
                            let tutorObj = {
                                name: data[0].name,
                                school: schoolName,
                                userid: data[0].userid
                            };
                            try {
                                tutors.set(data[0].dsid, tutorObj);
                                try {
                                    tutors.instantIndex(data[0].dsid);
                                    status = true;
                                } catch (e) {
                                    // Error handling
                                    logUtil.info("Error in catch on try tutors set - " + JSON.stringify(e));
                                    status = false;
                                }
                            } catch (e) {
                                // Error handling
                                logUtil.info('Error in  tutors.set- ' + JSON.stringify(e));
                                status = false;
                            }
                        } else {
                            try {

                                tutors.add(tutorObj, (err, data) => {
                                    if (err) {
                                        // handle error
                                        logUtil.info('Error in tutors add - ' + JSON.stringify(err));
                                        status = false;
                                        return status;
                                    } else {
                                        //Will update the index right on - use reducer to initiate store change.                     
                                        try {
                                            tutors.instantIndex(data.dsid);
                                            status = true;
                                        } catch (e) {
                                            // Error handling
                                            logUtil.info('Error - in store instant index after areas ADD - ' + JSON.stringify(e));
                                            status = false;
                                            return status;
                                        }
                                    }
                                });
                            } catch (e) {
                                // Error handling
                                logUtil.info('Error caught in tutors ADD method - ' + JSON.stringify(e));
                                status = false;
                                return status;
                            }
                        }
                    });
                } catch (error) {
                    // Error handling
                    logUtil.info('addTutor - findTutor.toArray  - Caught error in toArray - ' + JSON.stringify(error));
                }
            }
            return status;
        },
        updateTutors: function (tutorId, schoolName) {
            /*
            Update tutors in tutors data storage
            */
            var
                tutorItems = tutors.find('ds.analyzed.userid:' + tutorId, 99999),
                status = false;

            if (tutorItems) {
                tutorItems.each((err, data) => {
                    if (err) {
                        logUtil.info('Error in tutorItems.each -' + JSON.stringify(err));
                    } else {
                        //New tutorObj - empty school.
                        let tutorObj = {
                            name: data.name,
                            school: "",
                            userid: data.userid
                        };
                        //Here we have to empty if students are present.
                        if (data.hasOwnProperty('students')) {
                            for (var i = 0; i < data.students.length; i++) {
                                //DSID of Students
                                let studentDsid = data.students[i],
                                    studentEntry = null;
                                try {
                                    studentEntry = students.get(studentDsid);
                                    if (studentEntry) {
                                        studentEntry.tutor = "";
                                        try {
                                            students.set(studentDsid, studentEntry);
                                            try {
                                                students.instantIndex(studentDsid);
                                            } catch (e) {
                                                // Error handling
                                                logUtil.info('Error in students instantindex - ' + JSON.stringify(e));
                                            }
                                        } catch (e) {
                                            // Error handling
                                            logUtil.info('Error in students set - ' + JSON.stringify(e));
                                        }
                                    }
                                } catch (e) {
                                    // Error handling
                                    logUtil.info('Error in students get - ' + JSON.stringify(e));
                                }

                            }
                            tutorObj.students = [];
                        }
                        try {
                            //Set new data.
                            tutors.set(data.dsid, tutorObj);
                            try {
                                //Index new data.
                                tutors.instantIndex(data.dsid);
                                status = true;
                            } catch (e) {
                                // Error handling
                                logUtil.info('Error in tutorItems tutors.instandIndex- ' + JSON.stringify(e));
                            }
                        } catch (e) {
                            // Error handling
                            logUtil.info('Error in tutors.set - ' + JSON.stringify(e));
                        }
                    }
                });
                /*
                Update tutors if in groups
                */
                var
                    tutorInGroup = groups.find('ds.analyzed.tutors:' + tutorId + ' AND ds.analyzed.school:' + schoolName, 99999),
                    findTutorResult = null;

                try {
                    findTutorResult = tutorInGroup.toArray();
                    if (findTutorResult.length) {
                        for (var i = 0; i < findTutorResult.length; i++) {
                            var
                                dsid = findTutorResult[i].dsid,
                                currentTutorPos = findTutorResult[i].tutors.map(function (e) { return e; }).indexOf(tutorId);

                            if (currentTutorPos > -1) {
                                //Remove existance and add to group.                              
                                findTutorResult[i].tutors.splice(currentTutorPos, 1);
                                try {
                                    groups.set(dsid, findTutorResult[i]);
                                    try {
                                        groups.instantIndex(dsid);
                                        status = true;
                                    } catch (e) {
                                        // Error handling
                                        logUtil.info('Error in groups instantindex - ' + JSON.stringify(e));
                                    }
                                } catch (e) {
                                    logUtil.info('Error in groups set - ' + JSON.stringify(e));
                                }
                                //Decrement of iteration
                                i--;
                            }
                        }
                    }
                } catch (error) {
                    // Error handling
                    logUtil.info('tutorInGroup - tutorInGroup.toArray  - Caught error in toArray - ' + JSON.stringify(error));
                }
            }
            return status;
        }
    };
});