(function () {
   'use strict';


   const
      appData = require('appData'),
      logUtil = require('LogUtil'),
      portletContextUtil = require('PortletContextUtil'),
      properties = require('Properties'),
      storage = require('storage'),
      regions = storage.getCollectionDataStore('regions'),
      areas = storage.getCollectionDataStore('areas'),
      regionsStorageHandler = require('/module/server/regionsStorageHandler'),
      areasStorageHandler = require('/module/server/areasStorageHandler'),
      userUtil = require('UserUtil'),
      router = require('router');

   const
      directoryGroup = appData.getNode('usergroup'),
      coachesGroup = appData.getNode('coachesgroup'),
      regionGroup = appData.getNode('regiongroup');
   //currentUserId = portletContextUtil.getCurrentUser().getIdentifier();

   function getAllfromStorage(dataStorage) {
      var result = dataStorage.find('*', 99999),
         listOfItems = null;
      try {
         listOfItems = result.toArray().sort(function (a, b) {
            a = a.name.toLowerCase();
            b = b.name.toLowerCase();
            if (a == b) return 0;
            return a < b ? -1 : 1;
         });
      } catch (e) {
         logUtil.info('Rendering error when trying to convert items to array - ' + JSON.stringify(e));
      }
      return listOfItems;
   }
   function iterateUsers(idArray) {
      //MembersArray is a true or false to check whether we are iterating over administrators.
      var userArray = [];
      for (var i = 0; i < idArray.length; i++) {
         userArray.push({
            name: properties.get(idArray[i], 'displayName'),
            id: idArray[i]
         });
      }
      userArray.sort((a, b) => a.name.localeCompare(b.name));

      return userArray;

   }

   //User Access
   var
      currentUserAccess = userUtil.isMemberOfGroup(directoryGroup),
      regionsList = getAllfromStorage(regions),
      areasList = getAllfromStorage(areas);

   /*
   Administrators - group node selected from config 
   */
   var
      memberIds = properties.get(directoryGroup, 'member'),
      members = null;

   if (memberIds) {
      members = iterateUsers(memberIds);
   }

   /*
   Region responsible - group node selected from config 
   */
   var
      regionIds = properties.get(regionGroup, 'member'),
      regionResponsible = null;

   if (regionIds) {
      regionResponsible = iterateUsers(regionIds);
   }
   /*
   Coaches - group node selected from config 
   */
   var
      coachesId = properties.get(coachesGroup, 'member'),
      coaches = null;

   if (coachesId) {
      coaches = iterateUsers(coachesId, false);
   }
   /*
   Views used in rendering.
   */
   router.get('/', function (req, res) {
      logUtil.info(currentUserAccess);
      if (currentUserAccess) {
         res.render('/', { members: members, regionResponsible: regionResponsible, regionsList: regionsList, responseSuccess: false, responseMessage: "", alertType: null });
      }
      else {
         if (memberIds) {
            res.send('Ni har inte behörighet att se detta material.');
         } else {
            res.send('Fel grupp utpekad i konfigurationen.');
         }
      }
   });
   router.get('/edit-region', function (req, res) {
      if (currentUserAccess) {
         res.render('/edit-region', { members: members, regionsList: regionsList });
      }
   });
   router.get('/add-area', function (req, res) {
      if (currentUserAccess) {
         res.render('/add-area', { coaches: coaches, regionsList: regionsList });
      }
   });
   router.get('/edit-area', function (req, res) {
      if (currentUserAccess) {
         res.render('/edit-area', { areasList: areasList });
      }
   });
   /*
   REGIONS - Methods used in SDK for updating Data Storage.
   */
   router.get('/get-region', function (req, res) {
      var fetchRegion = regionsStorageHandler.fetchRegion(req);
      res.json({ region: fetchRegion, members: regionResponsible });
   });
   router.post('/post-region', function (req, res) {
      var postRegion = regionsStorageHandler.postRegion(req);

      res.json({ message: postRegion.message, success: postRegion.success });
   });
   router.put('/put-region', function (req, res) {
      var
         updateRegion = regionsStorageHandler.updateRegion(req),
         listOfRegions = getAllfromStorage(regions);
      res.json({ updateRegion: updateRegion, regionsList: listOfRegions });
   });
   router.get('/get-area', function (req, res) {
      var fetchArea = areasStorageHandler.fetchArea(req);

      res.json({ fetchArea: fetchArea, coaches: coaches, regionsList: regionsList });
   });
   router.post('/post-area', function (req, res) {
      var getAreas = areasStorageHandler.postArea(req);

      res.json({ message: getAreas.message, success: getAreas.success });
   });
   router.put('/put-area', function (req, res) {
      var
         updateArea = areasStorageHandler.updateArea(req),
         listOfRegions = getAllfromStorage(regions),
         listOfAreas = getAllfromStorage(areas);

      res.json({ updateArea: updateArea, areasList: listOfAreas, coaches: coaches, regionsList: listOfRegions });
   });
}());