// reducer.js
define(function (require) {
    'use strict';

    var _ = require('underscore');

    var reducer = function (state, action) {
        switch (action.type) {
            case 'EDITED_REGIONS':
                return _.extend({}, state, { regionsList: action.regionsList, responseSuccess: action.responseSuccess, responseMessage: action.responseMessage, alertType: action.alertType });
            case 'EDITED_AREAS':
                return _.extend({}, state, { areasList: action.areasList, regionsList: action.regionsList, responseSuccess: action.responseSuccess, responseMessage: action.responseMessage, alertType: action.alertType });
            default:
                return state;
        }
    };

    return reducer;
});