define(function (require) {
   'use strict';

   var
      _ = require('underscore'),
      $ = require('jquery'),
      Component = require('Component'),
      template = require('/template/main'),
      editRegionTemplate = require('/template/edit-region'),
      addAreaTemplate = require('/template/add-area'),
      editAreaTemplate = require('/template/edit-area');

   return Component.extend({
      getTemplate: function () {
         if (this.state.route === '/edit-region') {
            return editRegionTemplate;
         } else if (this.state.route === '/add-area') {
            return addAreaTemplate;
         } else if (this.state.route === '/edit-area') {
            return editAreaTemplate;
         } else {
            return template;
         }
      },
      filterState: function (state) {
         return _.extend({}, { members: state.members, regionResponsible: state.regionResponsible, regionsList: state.regionsList, areasList: state.areasList, responseSuccess: state.responseSuccess, responseMessage: state.responseMessage, alertType: state.alertType });
      }
   });
});