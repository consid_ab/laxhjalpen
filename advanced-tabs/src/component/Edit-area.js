define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        $ = require('jquery'),
        Component = require('Component'),
        router = require('router'),
        requester = require('requester'),
        generalFunctions = require('/module/client/generalFunctions'),
        template = require('/template/edit-area-content'),
        store = require('store');

    return Component.extend({

        template: template,

        events: {
            dom: {
                'change select.lax-form__select-area': 'findArea',
                'click button[data-modal-dialog-dismiss]': 'useModal',
                'click i.lax-icon__close': 'useModal',
                'change select.lax-form__select-coach': 'changeResponsiblePerson',
                'submit .lax-form__change-area': 'putArea',
            },
            self: {
                'state:changed': 'render'
            },
            store: 'handleStoreUpdate'
        },
        handleStoreUpdate: function (newState) {
            this.setState(newState);
        },
        //General functions
        useModal: function () {
            generalFunctions.handleModal('#edit-area-form')
        },
        //Doesn't work as general.
        changeResponsiblePerson: function (e) {
            var
                personHtml = '',
                userId = $(e.currentTarget)('option:selected').data('user-id') || null,
                name = $(e.currentTarget).find('option:selected').val();

            personHtml += '<ul class="lax-list"><li class="lax-list__item" data-id=' + userId + '>' + name + '</li></ul>';

            $('.lax-modal-body__content--persons').find('.lax-list').remove();
            $('.lax-modal-body__content--persons').append(personHtml);
        },
        //Component specific
        findArea: function (e) {
            e.preventDefault();
            if ($('.lax-alert').length) {
                $('.lax-alert').remove();
            }

            var html = "";

            requester.doGet({
                url: router.getUrl('/get-area'),
                data: {
                    dsid: $(e.currentTarget).find('option:selected').data('dsid')
                },
                context: this
            }).done(function (response) {
                if (response) {
                    html +=
                        '<div class="lax-modal-wrapper">' +
                        '<div id="edit-area-form" class="env-modal-dialog env-modal-dialog--show" role="dialog" aria-labelledby="myDialog" aria-hidden="true" tabindex="-1" style="background-color:rgba(0,0,0,.5);">' +
                        '<div class="env-modal-dialog__dialog">' +
                        '<section class="env-modal-dialog__content">' +
                        '<header class="env-modal-dialog__header lax-modal-dialog__header">' +
                        '<h6 class="env-text env-modal-dialog__header__title">Ändra område</h6>' +
                        '<i class="fa fa-times fa-lg lax-icon__close" aria-hidden="true"></i>' +
                        '</header>' +
                        '<form class="lax-form env-form lax-form__change-area">' +
                        '<div class="env-form">' +
                        '<div class="env-modal-dialog__body">' +
                        '<div class="lax-modal-body__content lax-modal-body__content--persons">' +
                        '<p class="env-text">Ansvarig coach</p>' +
                        '<ul class="lax-list"><li class="lax-list__item" data-id="' + response.fetchArea.coachId + '">' + response.fetchArea.coach + '</li></ul>' +
                        '</div>' +
                        '<div class="lax-modal-body__content">' +
                        '<div class="env-form-element__control">' +
                        '<label class="env-form-element__label" for="add-area-name">Namn</label>' +
                        '<input class="env-form-input" type="text" id="add-area-name" name="area" data-dsid=' + response.fetchArea.dsid + ' value="' + response.fetchArea.name + '" required readonly title="Namn går inte att redigera">' +
                        '</div>' +
                        '</div>' +
                        '<div class="lax-modal-body__content">' +
                        '<label for="edit-region-select" class="env-form-element__label">Region</label>' +
                        '<div class="env-form-element__control">';
                    let regionIndexPos = response.regionsList.map(function (e) { return e.dsid; }).indexOf(response.fetchArea.region);

                    if (regionIndexPos > -1) {
                        html += '<input class="env-form-input lax-form__select-region" id="edit-region-select" name="region" data-dsid=' + response.regionsList[regionIndexPos].dsid + ' required readonly title="Region går inte att redigera" value="' + response.regionsList[regionIndexPos].name + '"/>';
                    }
                    html +=
                        '</div>' +
                        '</div>' +
                        '<div class="lax-modal-body__content">' +
                        '<label for="select-coach" class="env-form-element__label">Ändra ansvarig coach</label>' +
                        '<div class="env-form-element__control">' +
                        '<select class="env-form-input lax-form__select-coach" id="add-coach-select" name="coach" required>';

                    let coachIndexPos = response.coaches.map(function (e) { return e.id; }).indexOf(response.fetchArea.coachId);

                    if (coachIndexPos > -1) {
                        html += '<option data-user-id=' + response.coaches[coachIndexPos].id + ' selected disabled hidden>' + response.coaches[coachIndexPos].name + '</option>';
                    }
                    for (var x = 0; x < response.coaches.length; x++) {
                        if (x == 0) {
                            html += '<option data-user-id="">Ingen ansvarig</option>';
                        }
                        html += '<option data-user-id=' + response.coaches[x].id + '>' + response.coaches[x].name + '</option>';
                    }
                    html +=
                        '</select>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<footer class="env-modal-dialog__footer lax-modal-dialog__footer">' +
                        '<button type="submit" class="lax-button env-button env-button--primary">Spara</button>' +
                        '<button type="button" data-modal-dialog-dismiss class="env-button">Avbryt</button>' +
                        '</footer>' +
                        '</div>' +
                        '</div>' +
                        '</section>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                    $(html).insertAfter($('.lax-form__edit-area'));
                }
            }).fail(function (response) {
                html += '<div class="env-alert lax-alert env-alert--danger" role="alert"><strong>' + response.message + '</strong></div>';
                $(html).insertAfter($('.lax-form__edit-area'));
            });
        },
        putArea: function (e) {
            e.preventDefault();
            requester.doPut({
                url: router.getUrl('/put-area'),
                data: {
                    area: this.$('input[name=area]').val(),
                    dsid: this.$('input[name=area]').data('dsid'),
                    region: this.$('input[name=region]').data('dsid'),
                    coach: this.$('select[name=coach] option:selected').data('user-id')
                },
                context: this
            }).done(function (response) {
                store.dispatch({
                    type: 'EDITED_AREAS',
                    areasList: response.areasList,
                    regionsList: response.regionsList,
                    responseSuccess: response.updateArea.success,
                    responseMessage: response.updateArea.message,
                    alertType: response.updateArea.alertType
                });
            }).fail(function (response) {
                //ADD ERROR HANDLING
                console.log(JSON.stringify(response));
            });
        },
        filterState: function (state) {
            return _.extend({}, { areasList: state.areasList, regionsList: state.regionsList, responseSuccess: state.responseSuccess, responseMessage: state.responseMessage, alertType: state.alertType });
        }
    });
});