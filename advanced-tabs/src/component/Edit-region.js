define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        $ = require('jquery'),
        Component = require('Component'),
        template = require('/template/edit-region-content'),
        generalFunctions = require('/module/client/generalFunctions'),
        router = require('router'),
        requester = require('requester'),
        store = require('store');

    return Component.extend({

        template: template,

        events: {
            dom: {
                'change select.lax-form__select-region': 'findRegion',
                'click button[data-modal-dialog-dismiss]': 'useModal',
                'click i.lax-icon__close': 'useModal',
                'change select.lax-form__select-responsible-person': 'changeResponsiblePerson',
                'submit .lax-form__edit-region': 'putRegion'
            },
            self: {
                'state:changed': 'render'
            },
            store: 'handleStoreUpdate'
        },
        handleStoreUpdate: function (newState) {
            this.setState(newState);
        },
        findRegion: function (e) {
            e.preventDefault();

            if ($('.lax-alert').length) {
                $('.lax-alert').remove();
            }

            var html = "";

            requester.doGet({
                url: router.getUrl('/get-region'),
                data: {
                    dsid: $(e.currentTarget).find('option:selected').data('dsid')
                },
                context: this
            }).done(function (response) {
                if (response) {
                    html +=
                        '<div class="lax-modal-wrapper">' +
                        '<div id="edit-region-form" class="env-modal-dialog env-modal-dialog--show" role="dialog" aria-labelledby="myDialog" aria-hidden="true" tabindex="-1" style="background-color:rgba(0,0,0,.5);">' +
                        '<div class="env-modal-dialog__dialog">' +
                        '<section class="env-modal-dialog__content">' +
                        '<header class="env-modal-dialog__header lax-modal-dialog__header">' +
                        '<h6 class="env-text env-modal-dialog__header__title">Ändra region</h6>' +
                        '<i class="fa fa-times fa-lg lax-icon__close" aria-hidden="true"></i>' +
                        '</header>' +
                        '<form class="lax-form env-form lax-form__edit-region">' +
                        '<div class="env-form">' +
                        '<div class="env-modal-dialog__body">' +
                        '<div class="lax-modal-body__content lax-modal-body__content--persons">' +
                        '<p class="env-text">Ansvarig</p>' +
                        '<ul class="lax-list"><li class="lax-list__item" data-id="' + response.region.id + '">' + response.region.name + '</li></ul>' +
                        '</div>' +
                        '<div class="lax-modal-body__content">' +
                        '<div class="env-form-element__control">' +
                        '<label class="env-form-element__label" for="region-name">Namn</label>' +
                        '<input class="env-form-input" type="text" id="region-name" name="regionName" title="Namn går inte att redigera" data-dsid=' + response.region.dsid + ' value="' + response.region.region + '" readonly required>' +
                        '</div>' +
                        '</div>' +
                        '<div class="lax-modal-body__content">' +
                        '<label for="responsible-person" class="env-form-element__label">Ändra ansvarig</label>' +
                        '<div class="env-form-element__control">' +
                        '<select class="env-form-input lax-form__select-responsible-person" id="select-region" name="responsible-person" required>';

                    let membersIndexPos = response.members.map(function (e) { return e.name; }).indexOf(response.region.name);

                    if (membersIndexPos > -1) {
                        html += '<option data-user-id=' + response.members[membersIndexPos].id + ' selected disabled hidden>' + response.members[membersIndexPos].name + '</option>';
                    }
                    for (var i = 0; i < response.members.length; i++) {
                        if (i == 0) {
                            html += '<option data-user-id="">Ingen ansvarig</option>';
                        }
                        html += '<option data-user-id=' + response.members[i].id + '>' + response.members[i].name + '</option>';
                    }
                    html +=
                        '</select>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<footer class="env-modal-dialog__footer lax-modal-dialog__footer">' +
                        '<button type="submit" class="lax-button env-button env-button--primary">Spara</button>' +
                        '<button type="button" data-modal-dialog-dismiss class="env-button">Avbryt</button>' +
                        '</footer>' +
                        '</div>' +
                        '</div>' +
                        '</section>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                    $(html).insertAfter($('.lax-form__edit-region'));
                }
            }).fail(function (response) {
                console.log('response not ok! - ' + JSON.stringify(response));
                html += '<div class="env-alert lax-alert env-alert--danger" role="alert"><strong>' + response.message + '</strong></div>';
                $(html).insertAfter($('.lax-form__edit-region'));
            });
        },
        useModal: function () {
            generalFunctions.handleModal('#edit-region-form');
        },
        changeResponsiblePerson: function (e) {
            var
                personHtml = '',
                userId = $(e.currentTarget).find('option:selected').data('user-id') || null,
                name = $(e.currentTarget).find('option:selected').val();

            personHtml += '<ul class="lax-list"><li class="lax-list__item" data-id=' + userId + '>' + name + '</li></ul>';

            $('.lax-modal-body__content--persons').find('.lax-list').remove();
            $('.lax-modal-body__content--persons').append(personHtml);
        },
        putRegion: function (e) {
            e.preventDefault();
            requester.doPut({
                url: router.getUrl('/put-region'),
                data: {
                    dsid: this.$('input[name=regionName]').data('dsid'),
                    region: this.$('input[name=regionName]').val(),
                    responsible: this.$('select[name=responsible-person] option:selected').data('user-id')
                },
                context: this
            }).done(function (response) {
                store.dispatch({
                    type: 'EDITED_REGIONS',
                    regionsList: response.regionsList,
                    responseSuccess: response.updateRegion.success,
                    responseMessage: response.updateRegion.message,
                    alertType: response.updateRegion.alertType
                });
            }).fail(function (response) {
                //ADD ERROR HANDLING
                console.log(JSON.stringify(response));
            });
        },
        filterState: function (state) {
            return _.extend({}, { members: state.members, regionResponsible: state.regionResponsible, regionsList: state.regionsList, responseSuccess: state.responseSuccess, responseMessage: state.responseMessage, alertType: state.alertType });
        }
    });
});