
define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        $ = require('jquery'),
        Component = require('Component'),
        router = require('router'),
        requester = require('requester'),
        template = require('/template/add-region');

    return Component.extend({

        template: template,
        events: {
            dom: {
                'submit .lax-form__add-region': 'addRegion'
            }
        },
        addRegion: function (e) {
            e.preventDefault();
            if ($('.lax-alert').length) {
                $('.lax-alert').remove();
            }

            var html = "";

            requester.doPost({
                url: router.getUrl('/post-region'),
                data: {
                    region: this.$('input[name=region]').val(),
                    responsible: this.$('select[name=responsible] option:selected').data('user-id')
                },
                context: this
            }).done(function (response) {
                if (response.success) {
                    html += '<div class="env-alert lax-alert env-alert--success" role="alert"><strong>' + response.message + '</strong></div>';
                } else {
                    html += '<div class="env-alert lax-alert env-alert--danger" role="alert"><strong>' + response.message + '</strong></div>';
                }
                $('.lax-form').append(html);
            }).fail(function (response) {
                html += '<div class="env-alert lax-alert env-alert--danger" role="alert"><strong>' + response.message + '</strong></div>';
                $('.lax-form').append(html);
            });
        },
        filterState: function (state) {
            return _.extend({}, { members: state.members, regionResponsible: state.regionResponsible });
        }
    });
});