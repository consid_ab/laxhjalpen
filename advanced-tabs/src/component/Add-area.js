
define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        Component = require('Component'),
        $ = require('jquery'),
        router = require('router'),
        requester = require('requester'),
        template = require('/template/add-area-content');

    return Component.extend({

        template: template,
        events: {
            dom: {
                'submit .lax-form__add-area': 'addArea'
            }
        },
        addArea: function (e) {
            e.preventDefault();
            if ($('.lax-alert').length) {
                $('.lax-alert').remove();
            }
            var html = "";

            requester.doPost({
                url: router.getUrl('/post-area'),
                data: {
                    area: this.$('input[name=area]').val(),
                    region: this.$('select[name=region] option:selected').data('dsid'),
                    coach: this.$('select[name=coach] option:selected').data('user-id') || ""
                },
                context: this
            }).done(function (response) {
                if (response.success) {
                    html += '<div class="env-alert lax-alert env-alert--success" role="alert"><strong>' + response.message + '</strong></div>';
                } else {
                    html += '<div class="env-alert lax-alert env-alert--danger" role="alert"><strong>' + response.message + '</strong></div>';
                }
                $('.lax-form').append(html);
            }).fail(function (response) {
                html += '<div class="env-alert lax-alert env-alert--danger" role="alert"><strong>' + response.message + '</strong></div>';
                $('.lax-form').append(html);
            });
        },
        filterState: function (state) {
            return _.extend({}, { coaches: state.coaches, regionsList: state.regionsList });
        }
    });
});