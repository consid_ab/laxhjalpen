define(function (require) {
    'use strict';

    var
        endecUtil = require('EndecUtil'),
        logUtil = require('LogUtil'),
        storage = require('storage'),
        properties = require('Properties'),
        regions = storage.getCollectionDataStore('regions'),
        resourceLocatorUtil = require('ResourceLocatorUtil');

    return {

        postRegion: function (req) {
            var
                message = "",
                success = false,
                findRegion = regions.find(req.params.region, 99999);

            try {
                findRegion.toArray((err, data) => {
                    if (data) {
                        if (data.length === 0) {

                            var regionObj = {
                                name: endecUtil.escapeXML(req.params.region),
                                responsible: endecUtil.escapeXML(req.params.responsible)
                            };

                            try {
                                regions.add(regionObj, (err, data) => {
                                    if (err) {
                                        // handle error
                                        logUtil.info('Error in regions add - ' + JSON.stringify(err));
                                        message += 'Ett fel inträffade i regions add - ' + JSON.stringify(err);
                                        success = false;
                                    } else {
                                        //Will update the index right on - use reducer to initiate store change.                     
                                        try {
                                            regions.instantIndex(data.dsid);
                                            message += "Din region är tillagd!";
                                            success = true;
                                        } catch (e) {
                                            // Error handling
                                            logUtil.info('Error - in store instant index after regions ADD - ' + JSON.stringify(e));
                                            message += 'Ett fel inträffade i instantIndex - ' + JSON.stringify(e);
                                            success = false;
                                        }
                                    }
                                });
                            } catch (e) {
                                // Error handling
                                logUtil.info('Error caught in regions ADD method - ' + JSON.stringify(e));
                                message += 'Ett fel inträffade i try regions.add - ' + JSON.stringify(e);
                                success = false;
                            }
                        } else {
                            message += "Regionen du försöker lägga till finns redan.";
                            success = false;
                        }
                    }
                });
            } catch (e) {
                // Error handling
                logUtil.info('Error in findRegion toArray - ' + JSON.stringify(e));
                message += JSON.stringify(e);
                success = false;
            }

            return {
                message: message,
                success: success
            };

        },
        fetchRegion: function (req) {
            var
                userId = null,
                regionName = null,
                regionResult = null,
                dsid = null;

            try {
                let region = regions.get(req.params.dsid);
                userId = region.responsible;
                regionName = region.name;
                dsid = req.params.dsid;

            } catch (e) {
                // Error handling
                logUtil.info('Error in getting DSID from regions - ' + JSON.stringify(e));
            }
            if (userId && userId !== "") {
                regionResult = {
                    name: properties.get(resourceLocatorUtil.getNodeByIdentifier(userId), 'displayName'),
                    id: userId,
                    region: regionName,
                    dsid: dsid
                };
            } else {
                regionResult = {
                    name: "Ingen ansvarig",
                    region: regionName,
                    dsid: dsid
                };
            }

            return regionResult;

        },
        updateRegion: function (req) {
            var
                message = "",
                success = false,
                alertType = null,
                dsid = req.params.dsid,
                regionData = {
                    responsible: req.params.responsible,
                    name: req.params.region
                };

            var findRegion = regions.find(req.params.region, 99999);

            try {
                findRegion.toArray((err, data) => {
                    if (data) {
                        if (data.length) {
                            if (dsid === data[0].dsid && regionData.name !== data[0].name) {
                                try {
                                    regions.set(dsid, regionData);
                                    try {
                                        regions.instantIndex(dsid);
                                        message += "Regionen har uppdaterats.";
                                        success = true;
                                        alertType = "success";
                                    } catch (e) {
                                        // Error handling
                                        message += "Ett fel inträffade, försök igen.";
                                        success = true;
                                        alertType = "error";
                                    }
                                } catch (e) {
                                    // Error handling
                                    logUtil.info('Error in regions set - ' + JSON.stringify(e));
                                    message += "Ett fel inträffade, försök igen.";
                                    success = true;
                                    alertType = "error";
                                }
                            } else {
                                if (dsid === data[0].dsid && regionData.name === data[0].name) {
                                    try {
                                        regions.set(dsid, regionData);
                                        try {
                                            regions.instantIndex(dsid);
                                            message += "Regionen har uppdaterats.";
                                            success = true;
                                            alertType = "success";
                                        } catch (e) {
                                            // Error handling
                                            message += "Ett fel inträffade, försök igen.";
                                            success = true;
                                            alertType = "error";
                                        }
                                    } catch (e) {
                                        // Error handling
                                        logUtil.info('Error in regions set - ' + JSON.stringify(e));
                                        message += "Ett fel inträffade, försök igen.";
                                        success = true;
                                        alertType = "error";
                                    }
                                } else {
                                    // Error handling                               
                                    message += "Ett fel inträffade, namnet finns redan.";
                                    success = true;
                                    alertType = "error";
                                }
                            }
                        }
                        else {
                            //Lägg till - finns ingen
                            try {
                                regions.set(dsid, regionData);
                                try {
                                    regions.instantIndex(dsid);
                                    message += "Regionen har uppdaterats.";
                                    success = true;
                                    alertType = "success";
                                } catch (e) {
                                    // Error handling
                                    message += "Ett fel inträffade, försök igen.";
                                    success = true;
                                    alertType = "error";
                                }
                            } catch (e) {
                                // Error handling
                                logUtil.info('Error in regions set - ' + JSON.stringify(e));
                                message += "Ett fel inträffade, försök igen.";
                                success = true;
                                alertType = "error";
                            }
                        }
                    }
                });
            } catch (e) {
                logUtil.info('e' + JSON.stringify(e));
                message += "Ett fel inträffade, försök igen.";
                success = true;
                alertType = "error";
            }

            return { message: message, success: success, alertType: alertType };
        }
        /*  deleteRegion: function (req) {
 
             //Här måste vi också ta bort regionen ur areas data storage
             var
                 message = "",
                 success = false,
                 alertType = null;
 
             try {
                 regions.remove(req.params.dsid);
                 try {
                     regions.instantIndex(req.params.dsid);
                     message += "Regionen togs bort.";
                     success = true;
                     alertType = "success";
                 } catch (e) {
                     // Error handling
                     message += "Ett fel inträffade, försök igen.";
                     success = false;
                     alertType = "error";
                 }
             } catch (e) {
                 // Error handling
                 logUtil.info('Error in regions remove - ' + JSON.stringify(e));
                 message += "Ett fel inträffade, försök igen.";
                 success = false;
                 alertType = "error";
             }
 
             return { message: message, success: success, alertType: alertType };
         } */
    };
});