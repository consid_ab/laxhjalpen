define(function (require) {
    'use strict';

    var
        endecUtil = require('EndecUtil'),
        logUtil = require('LogUtil'),
        storage = require('storage'),
        resourceLocatorUtil = require('ResourceLocatorUtil'),
        properties = require('Properties'),
        areas = storage.getCollectionDataStore('areas');

    return {

        postArea: function (req) {
            logUtil.info(JSON.stringify(req));
            var
                message = "",
                success = false,
                findArea = areas.find(req.params.area, 99999);

            try {
                findArea.toArray((err, data) => {
                    if (data) {
                        if (data.length === 0) {

                            var areaObj = {
                                name: endecUtil.escapeXML(req.params.area),
                                region: endecUtil.escapeXML(req.params.region),
                                coach: endecUtil.escapeXML(req.params.coach)
                            };

                            try {
                                areas.add(areaObj, (err, data) => {
                                    if (err) {
                                        // handle error
                                        logUtil.info('Error in areas add - ' + JSON.stringify(err));
                                        message += 'Ett fel inträffade i areas add - ' + JSON.stringify(err);
                                        success = false;
                                    } else {
                                        //Will update the index right on - use reducer to initiate store change.                     
                                        try {
                                            areas.instantIndex(data.dsid);
                                            message += "Ditt område är tillagt!";
                                            success = true;
                                        } catch (e) {
                                            // Error handling
                                            logUtil.info('Error - in store instant index after areas ADD - ' + JSON.stringify(e));
                                            message += 'Ett fel inträffade i instantIndex - ' + JSON.stringify(e);
                                            success = false;
                                        }
                                    }
                                });
                            } catch (e) {
                                // Error handling
                                logUtil.info('Error caught in areas ADD method - ' + JSON.stringify(e));
                                message += 'Ett fel inträffade i try areas.add - ' + JSON.stringify(e);
                                success = false;
                            }
                        } else {
                            message += "Området du försöker lägga till finns redan.";
                            success = false;
                        }
                    }
                });
            } catch (e) {
                // Error handling
                logUtil.info('Error in findArea toArray - ' + JSON.stringify(e));
                message += JSON.stringify(e);
                success = false;
            }

            return {
                message: message,
                success: success
            };
        },
        fetchArea: function (req) {
            var
                coachId = null,
                areaName = null,
                regionId = null,
                areaResult = null,
                dsid = null;

            try {

                let area = areas.get(req.params.dsid);
                logUtil.info('area ' + JSON.stringify(area));

                areaName = area.name;
                coachId = area.coach;
                regionId = area.region; //GER ID
                dsid = req.params.dsid;

            } catch (e) {
                // Error handling
                logUtil.info('Error in getting DSID from areas - ' + JSON.stringify(e));
            }
            if (coachId && coachId !== "") {
                areaResult = {
                    name: areaName,
                    region: regionId,
                    coach: properties.get(resourceLocatorUtil.getNodeByIdentifier(coachId), 'displayName'),
                    coachId: coachId,
                    dsid: dsid
                };
            } else {
                areaResult = {
                    name: areaName,
                    region: regionId,
                    coach: "Ingen ansvarig",
                    coachId: "",
                    dsid: dsid
                };
            }

            return areaResult;

        },
        updateArea: function (req) {
            var
                message = "",
                success = false,
                alertType = null,
                dsid = req.params.dsid,
                areaData = {
                    name: req.params.area,
                    region: req.params.region,
                    coach: req.params.coach
                };

            var findArea = areas.find(req.params.area, 99999);

            try {
                findArea.toArray((err, data) => {
                    if (data) {
                        if (data.length) {
                            if (dsid === data[0].dsid && areaData.name !== data[0].name) {
                                try {
                                    areas.set(dsid, areaData);
                                    try {
                                        areas.instantIndex(dsid);
                                        message += "Området har uppdaterats.";
                                        success = true;
                                        alertType = "success";
                                    } catch (e) {
                                        // Error handling
                                        message += "Ett fel inträffade, försök igen.";
                                        success = true;
                                        alertType = "error";
                                    }
                                } catch (e) {
                                    // Error handling
                                    logUtil.info('Error in areas set - ' + JSON.stringify(e));
                                    message += "Ett fel inträffade, försök igen.";
                                    success = true;
                                    alertType = "error";
                                }
                            } else {
                                if (dsid === data[0].dsid && areaData.name === data[0].name) {
                                    try {
                                        areas.set(dsid, areaData);
                                        try {
                                            areas.instantIndex(dsid);
                                            message += "Området har uppdaterats.";
                                            success = true;
                                            alertType = "success";
                                        } catch (e) {
                                            // Error handling
                                            message += "Ett fel inträffade, försök igen.";
                                            success = true;
                                            alertType = "error";
                                        }
                                    } catch (e) {
                                        // Error handling
                                        logUtil.info('Error in areas set - ' + JSON.stringify(e));
                                        message += "Ett fel inträffade, försök igen.";
                                        success = true;
                                        alertType = "error";
                                    }
                                } else {
                                    // Error handling                               
                                    message += "Ett fel inträffade, namnet finns redan.";
                                    success = true;
                                    alertType = "error";
                                }
                            }
                        }
                        else {
                            //Lägg till - finns ingen
                            try {
                                areas.set(dsid, areaData);
                                try {
                                    areas.instantIndex(dsid);
                                    message += "Området har uppdaterats.";
                                    success = true;
                                    alertType = "success";
                                } catch (e) {
                                    // Error handling
                                    message += "Ett fel inträffade, försök igen.";
                                    success = true;
                                    alertType = "error";
                                }
                            } catch (e) {
                                // Error handling
                                logUtil.info('Error in areas set - ' + JSON.stringify(e));
                                message += "Ett fel inträffade, försök igen.";
                                success = true;
                                alertType = "error";
                            }
                        }
                    }
                });
            } catch (e) {
                logUtil.info('e' + JSON.stringify(e));
                message += "Ett fel inträffade, försök igen.";
                success = true;
                alertType = "error";
            }

            return { message: message, success: success, alertType: alertType };
        },

    };
});