define(function (require) {
    'use strict';

    var $ = require('jquery');

    return {

        handleModal: function (id) {
            $(id).toggleClass(function () {
                $(id).removeClass('env-modal-dialog--show');
                $('.lax-modal-wrapper').remove();
            });
        }
    };
});