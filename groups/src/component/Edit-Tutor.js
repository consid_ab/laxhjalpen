define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        $ = require('jquery'),
        Component = require('Component'),
        template = require('/template/edit-tutor-content');

    return Component.extend({

        template: template,

        onRendered: function () {
            $('#students-select-multiple').select2({
                placeholder: "Inga elever valda",
                allowClear: true,
                closeOnSelect: false,
                width: 'resolve',
                language: {
                    noResults: function () {
                        return "Hittade inga elever.";
                    },

                },
            });
        },
        filterState: function (state) {
            return _.extend({}, { tutorEntry: state.tutorEntry, queryparams: state.queryparams, availableStudents: state.availableStudents, success: state.success, alertType: state.alertType, message: state.message });
        }
    });
});