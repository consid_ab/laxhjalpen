define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        $ = require('jquery'),
        Component = require('Component'),
        router = require('router'),
        requester = require('requester'),
        store = require('store'),
        template = require('/template/search-groups-content');

    return Component.extend({

        template: template,

        events: {
            dom: {
                'submit .lax-form__search-groups': 'handleSearch'
            },
            router: {
                'query:changed': 'getGroups'
            },
            self: {
                'state:changed': 'render'
            },
            store: 'handleStoreChange'
        },
        onRendered: function () {

            $('#select-school').select2({
                placeholder: "Välj eller sök skola",
                allowClear: true,
                width: 'resolve',
                language: {
                    noResults: function () {
                        return "Hittade inga skolor.";
                    }
                }
            });
        },
        handleStoreChange: function (newState) {
            this.setState(newState);
        },
        handleSearch: function (e) {
            e.preventDefault();
            var
                queryString = "",
                obj = {};

            $(".lax-form__search-groups :input").each(function (i) {
                var keyName;

                if ($(this)[0].tagName === "SELECT") {
                    if ($('option:selected', this).val() !== "") {
                        keyName = $(this)[0].name;
                        obj[keyName] = $('option:selected', this).val();
                    } else {
                        keyName = $(this)[0].name;
                        obj[keyName] = "*";
                    }
                } else {
                    if ($(this)[0].tagName !== "BUTTON") {
                        if ($(this).val() !== "") {
                            keyName = $(this)[i].name;
                            obj[keyName] = $(this).val();
                        }
                        else {
                            keyName = $(this)[0].name;
                            obj[keyName] = "*";
                        }
                    }
                }
                i++;
            });
            queryString += Object.keys(obj).map((key) => { return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]); }).join('&');

            //Route the form action.
            router.navigate(e.currentTarget.action, {
                queryParams: { query: queryString }
            });
        },
        getGroups: function (options) {
            if ($('.env-alert').length) {
                $('.env-alert').remove();
            }
            var queryObject = Object.fromEntries(new URLSearchParams(options.queryParams.query));

            requester.doGet({
                url: router.getUrl('/fetch-groups'),
                data: {
                    data: JSON.stringify(queryObject),
                    query: options.queryParams.query
                },
                context: this
            }).done(function (response) {
                store.dispatch({
                    type: 'GET_GROUPS',
                    groups: response.groups,
                    name: response.name,
                    selectedSchool: response.selectedSchool,
                    editGroup: response.editGroup
                });
            });
        },
        filterState: function (state) {
            return _.extend({}, { groups: state.groups, editGroup: state.editGroup, name: state.name, selectedSchool: state.selectedSchool, schools: state.schools, schoolsArea: state.schoolsArea });
        }
    });
});