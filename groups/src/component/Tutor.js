define(function (require) {
    'use strict';

    var
        Component = require('Component'),
        router = require('router'),
        template = require('/template/tutor');

    return Component.extend({

        template: template,

        tagName: 'li',

        className: 'lax-tutor-list__item'

    });
});