define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        ListComponent = require('ListComponent');

    return ListComponent.extend({
        tagName: 'ul',

        className: 'lax-tutor-list',

        childProperty: 'tutors',

        childComponentPath: 'Tutor',
        
        filterState: function (state, options) {
            return Object.assign({}, options);
        }
    });
});