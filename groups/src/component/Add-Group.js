define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        $ = require('jquery'),
        router = require('router'),
        requester = require('requester'),
        store = require('store'),
        Component = require('Component'),
        template = require('/template/add-group');

    return Component.extend({

        template: template,
        events: {
            dom: {
                'change select#select-school': 'findTutors'
            },
            self: {
                'state:changed': 'render'
            },
            store: 'handleStoreUpdate'
        },
        handleStoreUpdate: function (newState) {
            this.setState(newState);
        },
        findTutors: function (e) {

            var
                school = $(e.currentTarget).val(),
                name = $(e.currentTarget).closest('.lax-form__add-tutor').find('input[name="name"]').val();

            requester.doGet({
                url: router.getStandaloneUrl('/get-tutors'),
                data: {
                    name: name,
                    school: school
                },
                context: this
            }).done(function (response) {
                store.dispatch({
                    type: 'SET_TUTORS',
                    tutors: response.tutors,
                    selectedSchool: response.selectedSchool,
                    name: response.name
                });
            });


        },
        onRendered: function () {
            $('#select-school').select2({
                placeholder: "Välj eller sök skola",
                width: 'resolve',
                language: {
                    noResults: function () {
                        return "Hittade inga skolor.";
                    }
                }
            });
            $('#tutors-select-multiple').select2({
                placeholder: "Välj läxhjälpare",
                closeOnSelect: false,
                width: 'resolve',
                language: {
                    noResults: function () {
                        return "Inga läxhjälpare tillgängliga.";
                    }
                },
            });
        },
        filterState: function (state) {
            return _.extend({}, { tutors: state.tutors, selectedSchool: state.selectedSchool, name: state.name, schools: state.schools, schoolsArea: state.schoolsArea, success: state.success, alertType: state.alertType, message: state.message });
        }
    });
});