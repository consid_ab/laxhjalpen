// reducer.js
define(function (require) {
    'use strict';

    var _ = require('underscore');

    var reducer = function (state, action) {
        switch (action.type) {
            case 'SET_TUTORS':
                return _.extend({}, state, { tutors: action.tutors, selectedSchool: action.selectedSchool, name: action.name });
            case 'GET_GROUPS':
                return _.extend({}, state, { groups: action.groups, selectedSchool: action.selectedSchool, name: action.name, editGroup: action.editGroup });
            default:
                return state;
        }
    };

    return reducer;
});