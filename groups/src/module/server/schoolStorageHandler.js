define(function (require) {
    'use strict';

    const
        logUtil = require('LogUtil'),
        storage = require('storage'),
        schools = storage.getCollectionDataStore('schools'),
        groups = storage.getCollectionDataStore('groups');

    return {
        getTutors: function (school) {
            var
                findSchool = schools.find(school, 99999),
                allTutors = null,
                result = null;

            try {
                result = findSchool.toArray();
                if (result.length) {
                    allTutors = result[0].tutors;

                    for (var i = 0; i < allTutors.length; i++) {

                        let tutorId = allTutors[i].id;
                        var tutorInGroups = groups.find('ds.analyzed.tutors:' + tutorId + ' AND ds.analyzed.school:' + school, 99999);

                        try {

                            var tutorGroupArr = tutorInGroups.toArray();

                            if (tutorGroupArr.length) {
                                allTutors.splice(i, 1);
                                //Decrement iteration after splice.
                                i--;
                            }
                        } catch (e) {
                            // Error handling
                            logUtil.info('Error in tutorInGroups toArray ' + JSON.stringify(e));
                        }
                    }
                    result = allTutors;
                }
            } catch (e) {
                // Error handling
                logUtil.info('Error in findSchool toArray ' + JSON.stringify(e));
            }
            return result;
        }
    };
});