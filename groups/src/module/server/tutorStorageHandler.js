define(function (require) {
    'use strict';

    const
        logUtil = require('LogUtil'),
        storage = require('storage'),
        students = storage.getCollectionDataStore('students'),
        tutors = storage.getCollectionDataStore('tutors');

    return {
        getTutor: function (tutorId) {

            var
                findTutor = tutors.find(tutorId, 99999),
                result = null;

            try {
                result = findTutor.toArray();
                if (result.length) {
                    for (var i = 0; i < result.length; i++) {
                        for (var x = 0; x < result[i].students.length; x++) {
                            //Fetch student from DSID
                            result[i].students[x] = students.get(result[i].students[x]);
                        }
                    }
                }
            } catch (e) {
                // Error handling
                logUtil.info('Error in findTutor toArray ' + JSON.stringify(e));
            }
            return result;
        },
        handleStudents: function (dsid, studentsList) {
            var
                result = null,
                message = "",
                success = false,
                alertType = null,
                tutorName = null;

            try {
                result = tutors.get(dsid);
                if (result) {
                    tutorName = result.name;
                    if (result.students) {
                        //Check if currentStudents is present i students array
                        //If not we update each student with empty string.
                        for (var i = 0; i < result.students.length; i++) {
                            //Har vi length
                            var updateStudent = null;
                            if (studentsList.length) {
                                if (studentsList.includes(result.students[i]) === false) {
                                    //Eleven finns inte i arrayen - vi skall sätta en tom sträng.                                 
                                    try {
                                        updateStudent = students.get(result.students[i]);
                                        if (updateStudent) {
                                            updateStudent.tutor = "";
                                            try {
                                                students.set(result.students[i], updateStudent);
                                                try {
                                                    students.instantIndex(result.students[i]);
                                                } catch (e) {
                                                    // Error handling
                                                    logUtil.info('Error in students SET for instantIndex in tutorstoragehandler - ' + JSON.stringify(e));
                                                }
                                            } catch (e) {
                                                logUtil.info('Error in students SET for updateStudent in tutorstoragehandler - ' + JSON.stringify(e));
                                            }
                                        }
                                    } catch (e) {
                                        logUtil.info('Error in students GET for updateStudent in tutorstoragehandler - ' + JSON.stringify(e));
                                    }

                                } else {
                                    logUtil.info('listan vi skickar innehåller - ' + result.students[i]);
                                }
                            } else {
                                //Vi har inte length!
                                //Uppdatera föregående studenter med tom sträng;                        
                                try {
                                    updateStudent = students.get(result.students[i]);
                                    if (updateStudent) {
                                        updateStudent.tutor = "";
                                        try {
                                            students.set(result.students[i], updateStudent);
                                            try {
                                                students.instantIndex(result.students[i]);
                                            } catch (e) {
                                                // Error handling
                                                logUtil.info('Error in students SET for instantIndex in tutorstoragehandler - ' + JSON.stringify(e));
                                            }
                                        } catch (e) {
                                            logUtil.info('Error in students SET for updateStudent in tutorstoragehandler - ' + JSON.stringify(e));
                                        }
                                    }
                                } catch (e) {
                                    logUtil.info('Error in students GET for updateStudent in tutorstoragehandler - ' + JSON.stringify(e));
                                }
                            }
                        }

                        //Set studentsList to update!
                        result.students = studentsList;
                        //Update each student with tutorName!
                        for (var x = 0; x < result.students.length; x++) {
                            let setTutorName = null;
                            try {
                                setTutorName = students.get(result.students[x]);
                                if (setTutorName) {
                                    setTutorName.tutor = tutorName;
                                    try {
                                        students.set(result.students[x], setTutorName);
                                        try {
                                            students.instantIndex(result.students[x]);
                                        } catch (e) {
                                            // Error handling
                                            logUtil.info('Error in students SET for instantIndex on new studentList - ' + JSON.stringify(e));
                                        }
                                    } catch (e) {
                                        logUtil.info('Error in students SET for setTutorName in tutorstoragehandler - ' + JSON.stringify(e));
                                    }
                                }
                            } catch (e) {
                                logUtil.info('Error in students GET for setTutorName in tutorstoragehandler - ' + JSON.stringify(e));
                            }
                        }
                        try {
                            tutors.set(dsid, result);
                            if (result) {
                                try {
                                    tutors.instantIndex(dsid);
                                    success = true;
                                    alertType = "success";
                                    message = "Läxhjälparens elever har uppdaterats!";
                                } catch (e) {
                                    // Error handling
                                    success = true;
                                    alertType = "error";
                                    message = "Läxhjälparens elever kunde inte uppdateras i databasen, försök igen.";
                                }
                            }
                        } catch (e) {
                            // Error handling
                            logUtil.info('Error in tutors set - ' + JSON.stringify(e));
                            message += "Ett fel inträffade, försök igen.";
                            success = true;
                            alertType = "error";
                        }
                    }
                }
            } catch (e) {
                // Error handling
                logUtil.info('Error in tutors set - ' + JSON.stringify(e));
                message += "Ett fel inträffade, hittade inte läxhjälparen i databasen, försök igen.";
                success = true;
                alertType = "error";
            }
            return { message: message, success: success, alertType: alertType };
        },
        updateTutorStudents: function (userId) {
            /*
            When function called upon - update all students (remove from array - set empty array)
            For every student update tutor to empty string.
            */
            var
                tutor = tutors.find('ds.analyzed.userid:' + userId, 99999),
                result = null,
                success = false;
            try {
                result = tutor.toArray();
                //can only be one entry.
                for (var i = 0; i < result.length; i++) {
                    var tutorStudents = result[i].students;
                    //Scenario beeing a tutor should be removed if he/she has students connected.
                    if (tutorStudents.length) {
                        for (var x = 0; x < tutorStudents.length; x++) {
                            //Get DSID for student.
                            var studentDsid = tutorStudents[x],
                                item = null;
                            //GET Student and set tutor to emtpy.
                            try {
                                item = students.get(studentDsid);
                                if (item) {
                                    item.tutor = "";
                                    try {
                                        students.set(studentDsid, item);
                                        success = true;
                                        try {
                                            students.instantIndex(studentDsid);
                                        } catch (e) {
                                            // Error handling
                                            logUtil.info('Error in student instantIndex ' + JSON.stringify(e));
                                        }
                                    } catch (e) {
                                        // Error handling
                                        logUtil.info('Error in students SET inside Students GET inside Tutors find ' + JSON.stringify(e));
                                    }
                                }
                            } catch (e) {
                                // Error handling
                                logUtil.info('Error in students GET inside Tutors find ' + JSON.stringify(e));
                            }
                        }
                    } else {
                        //Scenario beeing a tutor can be added and have an empty array - this is correct.                        
                        success = true;
                    }
                    //Set students array to empty.
                    result[i].students = [];
                    //Update tutor
                    try {
                        tutors.set(result[i].dsid, result[i]);
                        try {
                            tutors.instantIndex(result[i].dsid);
                        } catch (e) {
                            // Error handling
                            logUtil.info('Error in tutors instantIndex ' + JSON.stringify(e));
                        }

                    } catch (e) {
                        // Error handling
                        logUtil.info('Error in tutors SET ' + JSON.stringify(e));
                    }

                }
            } catch (e) {
                // Error handling
                logUtil.info('Error in tutors find ' + JSON.stringify(e));
            }
            return success;
        }
    };
});