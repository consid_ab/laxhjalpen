define(function (require) {
    'use strict';

    const
        logUtil = require('LogUtil'),
        storage = require('storage'),
        students = storage.getCollectionDataStore('students');

    return {

        getStudents: function (school) {
            var
                findStudents = students.find('ds.analyzed.school:' + school + ' AND -ds.analyzed.tutor:["" TO *]', 99999),
                result = null;

            try {
                result = findStudents.toArray();
            } catch (e) {
                // Error handling
                logUtil.info('Error in findStudents toArray ' + JSON.stringify(e));
                result = [];
            }
            return result;
        }

    };
});