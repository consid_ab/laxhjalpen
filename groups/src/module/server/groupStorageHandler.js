define(function (require) {
    'use strict';

    const
        logUtil = require('LogUtil'),
        storage = require('storage'),
        resourceLocatorUtil = require('ResourceLocatorUtil'),
        properties = require('Properties'),
        schoolStorageHandler = require('/module/server/schoolStorageHandler'),
        tutorStorageHandler = require('/module/server/tutorStorageHandler'),
        groups = storage.getCollectionDataStore('groups');

    return {
        postGroup: function (req) {

            var
                groupsObj = {
                    name: req.params.name,
                    school: req.params.school,
                    tutors: req.params.tutors
                },
                message = "",
                success = false,
                alertType = null;

            try {
                groups.add(groupsObj, (err, data) => {
                    if (err) {
                        // handle error
                        logUtil.info('Error in groups add - ' + JSON.stringify(err));
                    } else {
                        try {
                            groups.instantIndex(data.dsid);
                            success = true;
                            alertType = "success";
                            message = "Gruppen är tillagd!";
                        } catch (e) {
                            // Error handling
                            logUtil.info('Error - in store instant index after groups ADD - ' + JSON.stringify(e));
                            success = true;
                            alertType = "error";
                            message = "Gruppen kunde inte uppdateras i databasen, försök igen.";
                        }
                    }
                });
            } catch (e) {
                // Error handling
                logUtil.info('Error caught in groups ADD method - ' + JSON.stringify(e));
                success = true;
                alertType = "error";
                message = "Gruppen kunde inte läggas till, försök igen.";
            }
            return { success: success, alertType: alertType, message: message };
        },
        getGroups: function (queryData, queryParams) {
            var
                dataObj = JSON.parse(queryData),
                searchStringArray = [],
                searchString = "",
                school = dataObj.school;

            for (let key in dataObj) {
                if (dataObj.hasOwnProperty(key)) {
                    let objectString = 'ds.analyzed.' + key + ':' + dataObj[key];
                    searchStringArray.push(objectString);
                }
            }
            if (searchStringArray.length) {
                for (var i = 0; i < searchStringArray.length; i++) {
                    if (i == searchStringArray.length - 1) {
                        searchString += searchStringArray[i] + '*';
                    } else {
                        searchString += searchStringArray[i] + '* AND ';
                    }
                }
                //Do search
                var
                    findGroups = groups.find(searchString, 99999),
                    result = null;

                try {
                    result = findGroups.toArray();
                    if (result.length) {
                        for (var x = 0; x < result.length; x++) {
                            var
                                tutorsList = result[x].tutors || [],
                                tutorArray = [];
                            for (var y = 0; y < tutorsList.length; y++) {
                                tutorArray.push({
                                    id: tutorsList[y],
                                    name: properties.get(resourceLocatorUtil.getNodeByIdentifier(tutorsList[y]), 'displayName'),
                                    queryparams: queryParams
                                });
                            }
                            result[x].tutors = tutorArray;
                            result[x].availableTutors = schoolStorageHandler.getTutors(school);
                            result[x].query = queryParams;
                        }
                    }
                } catch (e) {
                    // Error handling
                    logUtil.info('Error in findGroups toArray ' + JSON.stringify(e));
                }
                return result;
            }
        },
        editGroup: function (req) {
            var
                groupName = req.params.name,
                currentGroupDsid = req.params.dsid,
                currentItem = null,
                sendingTutors = req.params.tutors;
            //First get DSID to get the current group.
            try {
                currentItem = groups.get(currentGroupDsid);

                var currentTutors = currentItem.tutors || [];
                //Iterate over existing tutors.
                for (var i = 0; i < currentTutors.length; i++) {
                    //Check if currentTutor is in the tutorArray we are sending.                                    
                    var currentTutorPos = sendingTutors.map(function (e) { return e; }).indexOf(currentTutors[i]);

                    if (currentTutorPos === -1) {
                        //Current tutor does not exist.
                        //Update tutor students if any.
                        let updateTutorStudents = tutorStorageHandler.updateTutorStudents(currentTutors[i]);

                        //Remove entry from exisiting.                        
                        currentTutors.splice(i, 1);
                        //Decrement iteration
                        i--;
                        try {
                            groups.set(currentGroupDsid, currentItem);
                            try {
                                groups.instantIndex(currentGroupDsid);
                            } catch (e) {
                                // Error handling
                                logUtil.info('Error in groups instantIndex ' + JSON.stringify(e));
                            }
                        } catch (e) {
                            // Error handling
                            logUtil.info('Error in groups set before instantIndex ' + JSON.stringify(e));
                        }

                    } else {
                        //Remove the existance of tutors - we are only adding necessary data.
                        sendingTutors.splice(sendingTutors[currentTutors], 1);
                    }
                }
                //Loop existing tutors and add them.
                if ((sendingTutors || []).length > 0) {
                    for (var x = 0; x < sendingTutors.length; x++) {
                        currentTutors.push(sendingTutors[x]);
                    }
                    groups.set(currentGroupDsid, currentItem);
                    try {
                        groups.instantIndex(currentGroupDsid);
                    } catch (e) {
                        // Error handling
                        logUtil.info('Error in groups instantIndex ' + JSON.stringify(e));
                    }
                }
                if (groupName !== "") {
                    currentItem.name = groupName;
                    groups.set(currentGroupDsid, currentItem);
                    try {
                        groups.instantIndex(currentGroupDsid);
                    } catch (e) {
                        // Error handling
                        logUtil.info('Error in groups on groupname instantIndex ' + JSON.stringify(e));
                    }
                }
            } catch (e) {
                logUtil.info('Error in groups GET ' + JSON.stringify(e));
            }

            return true;

        }
    };
});