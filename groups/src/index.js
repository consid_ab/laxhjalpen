(function () {
   'use strict';

   var
      logUtil = require('LogUtil'),
      router = require('router'),
      groupStorageHandler = require('/module/server/groupStorageHandler'),
      schoolStorageHandler = require('/module/server/schoolStorageHandler'),
      tutorStorageHandler = require('/module/server/tutorStorageHandler'),
      studentStorageHandler = require('/module/server/studentStorageHandler'),
      storage = require('storage'),
      schools = storage.getCollectionDataStore('schools');

   /*
   Get all items in a datastorage and return in alphabetical order.
   */
   function getAllfromStorage(dataStorage) {
      var
         result = dataStorage.find('*', 99999),
         listOfItems = null;
      try {
         listOfItems = result.toArray().sort(function (a, b) {
            a = a.name.toLowerCase();
            b = b.name.toLowerCase();
            if (a == b) return 0;
            return a < b ? -1 : 1;
         });
      } catch (e) {
         logUtil.info('Error when trying to convert items to array in getAllFromStorage - ' + JSON.stringify(e));
      }
      return listOfItems;
   }
   /*
   Get all unique school locations.
   Sort alphabetically
   */
   function getSchoolAreas(list) {
      var schoolsArea = [];
      for (var i = 0; i < list.length; i++) {
         if (schoolsArea.indexOf(list[i].area) === -1) {
            schoolsArea.push(list[i].area);
         }
      }
      return schoolsArea.sort();
   }
   /*
   Views
   */
   router.get('/', function (req, res) {

      var
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools);

      res.render('/', { tutors: null, name: null, schools: availableSchools, schoolsArea: schoolsArea, success: false, alertType: null, message: "" });
   });

   //View for managing a tutor from a group.
   router.get('/manage-tutor', function (req, res) {
      var
         tutorEntry = tutorStorageHandler.getTutor(req.params.tutorId),
         availableStudents = studentStorageHandler.getStudents(tutorEntry[0].school),
         queryparams = req.params.queryparams;

      res.render('/edit-tutor', { tutorEntry: tutorEntry, queryparams: queryparams, availableStudents: availableStudents, success: false, alertType: null, message: "" });
   });

   /*
   Methods
   */
   router.post('/add-group', function (req, res) {
      var
         addGroup = groupStorageHandler.postGroup(req),
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools);

      res.render('/', { tutors: null, selectedSchool: null, name: null, schools: availableSchools, schoolsArea: schoolsArea, success: addGroup.success, alertType: addGroup.alertType, message: addGroup.message });
   });

   router.get('/search-groups', function (req, res) {

      var
         groups = null,
         name = null,
         selectedSchool = null,
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools);

      if (req.params.hasOwnProperty('query')) {
         var
            queryparams = req.params.query,
            queryObject = queryparams.split("&").reduce(function (prev, curr) {
               let p = curr.split("=");
               prev[p[0]] = decodeURIComponent(p[1]);
               return prev;
            }, {});
         groups = groupStorageHandler.getGroups(JSON.stringify(queryObject), queryparams, queryObject.school);
         name = queryObject.name;
         selectedSchool = queryObject.school;
      }
      if (groups) {
         groups = groups.sort(function (a, b) {
            return a.name - b.name;
         });
      }
      res.render('/search-groups', { groups: groups, selectedSchool: selectedSchool, name: name, schools: availableSchools, schoolsArea: schoolsArea, editGroup: null });
   });

   router.get('/fetch-groups', function (req, res) {
      var
         groups = groupStorageHandler.getGroups(req.params.data, req.params.query),
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools);

      var groupsData = JSON.parse(req.params.data);
      if (groups) {
         groups = groups.sort(function (a, b) {
            return a.name - b.name;
         });
      }
      res.json({ groups: groups, selectedSchool: groupsData.school, name: groupsData.name, schools: availableSchools, schoolsArea: schoolsArea, editGroup: null });
   });
   router.post('/edit-group', function (req, res) {

      var editGroup = groupStorageHandler.editGroup(req);

      var
         groups = null,
         name = null,
         selectedSchool = null,
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools);

      if (req.params.hasOwnProperty('query')) {
         var
            queryparams = req.params.query,
            queryObject = queryparams.split("&").reduce(function (prev, curr) {
               let p = curr.split("=");
               prev[p[0]] = decodeURIComponent(p[1]);
               return prev;
            }, {});
         groups = groupStorageHandler.getGroups(JSON.stringify(queryObject), queryparams, queryObject.school);
         name = queryObject.name;
         selectedSchool = queryObject.school;
      }
      if (groups) {
         groups = groups.sort(function (a, b) {
            return a.name - b.name;
         });
      }
      res.render('/search-groups', { groups: groups, selectedSchool: selectedSchool, name: name, schools: availableSchools, schoolsArea: schoolsArea, editGroup: editGroup });

   });
   router.get('/get-tutors', function (req, res) {

      var tutors = schoolStorageHandler.getTutors(req.params.school);

      res.json({ tutors: tutors, selectedSchool: req.params.school, name: req.params.name });
   });

   router.post('/handle-tutor', function (req, res) {
      var
         tutordsid = req.params.tutordsid,
         studentsList = null,
         queryparams = req.params.queryparams;

      if (req.params.hasOwnProperty('students')) {
         studentsList = req.params.students;
      } else {
         studentsList = [];
      }
      var
         handleStudents = tutorStorageHandler.handleStudents(tutordsid, studentsList),
         tutorEntry = tutorStorageHandler.getTutor(tutordsid),
         availableStudents = studentStorageHandler.getStudents(tutorEntry[0].school);

      res.render('/edit-tutor', { tutorEntry: tutorEntry, queryparams: queryparams, availableStudents: availableStudents, success: handleStudents.success, alertType: handleStudents.alertType, message: handleStudents.message });

   });

   router.get('/handle-tutor', function (req, res) {
      //If refreshing page after update - "redirect" to start.
      var
         availableSchools = getAllfromStorage(schools),
         schoolsArea = getSchoolAreas(availableSchools);

      res.render('/', { tutors: null, name: null, schools: availableSchools, schoolsArea: schoolsArea, success: false, alertType: null, message: "" });

   });

}());