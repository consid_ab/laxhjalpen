define(function (require) {
   'use strict';

   var
      _ = require('underscore'),
      Component = require('Component'),
      template = require('/template/main'),
      searchTemplate = require('/template/search-groups'),
      tutorTemplate = require('/template/edit-tutor');

   return Component.extend({

      getTemplate: function () {
         if (this.state.route === '/search-groups') {
            return searchTemplate;
         } else if (this.state.route === '/edit-tutor') {
            return tutorTemplate;
         }
         else {
            return template;
         }
      },
      filterState: function (state) {
         return _.extend({}, {
            tutors: state.tutors,
            groups: state.groups,
            selectedSchool: state.selectedSchool,
            name: state.name,
            schools: state.availableSchools,
            schoolsArea: state.schoolsArea,
            success: state.success,
            alertType: state.alertType,
            message: state.message
         });
      }
   });
});