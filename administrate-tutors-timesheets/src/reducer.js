// reducer.js
define(function (require) {
    'use strict';

    var _ = require('underscore');

    var reducer = function (state, action) {
        switch (action.type) {
            case 'EDIT_TIMESHEET':
                return _.extend({}, state, { tutors: action.tutors, success: action.success, message: action.message, alertType: action.alertType });
            default:
                return state;
        }
    };

    return reducer;
});