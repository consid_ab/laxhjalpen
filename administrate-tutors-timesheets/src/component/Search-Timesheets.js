define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        Component = require('Component'),
        $ = require('jquery'),
        template = require('/template/search-timesheets');

    return Component.extend({

        template: template,

        onRendered: function () {
            $('#select-school').select2({
                placeholder: "Välj eller sök skola",
                allowClear: true,
                width: 'resolve',
                language: {
                    noResults: function () {
                        return "Hittade inga skolor.";
                    }
                }
            });
        },
        
        tagName: 'div',

        className: 'lax-accordion__wrapper env-list__item',
        
        filterState: function(state) {
            return _.extend({}, {
                schoolAreas: state.schoolAreas,
                tutorName: state.tutorName,
                schoolId: state.schoolId,
                tutors: state.tutors
            });
        }
        
    });
});