define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        ListComponent = require('ListComponent');

    return ListComponent.extend({
        tagName: 'div',

        className: 'env-list env-list-dividers--bottom',

        childProperty: 'tutors',

        childComponentPath: 'Tutor',

        attributes: function () {
            return {
                'id': 'lax-accordion'
            };
        },
        filterState: function (state, options) {
            return _.extend({}, {
                tutors: options.tutors
            });
        }
    });
});