define(function (require) {
    'use strict';

    var
        Component = require('Component'),
        $ = require('jquery'),
        template = require('/template/tutor');

    return Component.extend({

        template: template,

        tagName: 'div',

        className: 'lax-accordion__wrapper env-list__item'
    });
});