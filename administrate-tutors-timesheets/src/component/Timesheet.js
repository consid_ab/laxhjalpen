define(function (require) {
    'use strict';

    var
        Component = require('Component'),
        template = require('/template/timesheet');

    return Component.extend({

        template: template,

        tagName: 'li',

        className: 'lax-timesheet__month-list-item',

    });
});