define(function (require) {
    'use strict';

    var
        _ = require('underscore'),
        ListComponent = require('ListComponent');

    return ListComponent.extend({
        tagName: 'ul',

        className: 'lax-timesheet__month-list',

        childProperty: 'timesheets',

        childComponentPath: 'Timesheet',

        filterState: function (state, options) {
            options.timesheets.sort(function (a, b) {
                return a.index - b.index;
            });

            return Object.assign({}, options);

        }
    });
});