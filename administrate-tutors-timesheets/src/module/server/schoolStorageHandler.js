define(function (require) {
    'use strict';

    const _ = require("underscore");
    const storage = require("storage");
    
    return {
        getAreasWithSchools: function() {
            const store = storage.getCollectionDataStore('schools');
            const schools = store.find('*', 10000).toArray();
            const areas = _.map(_.groupBy(schools, x => x.area ? x.area : "Övrigt"), (value, key) => ({
                area: key,
                schools: value
            })).sort((a, b) => {
            if (a.area === b.area) return 0;
            // A hack to move Övrigt last in the list, due to swedish locale not being available
            if (a.area === "Övrigt") return 1;
            if (b.area === "Övrigt") return -1;
                return a.area.localeCompare(b.area);
            });
            return areas;
        }
    };
});