define(function (require) {
    'use strict';

    var
        _ = require("underscore"),
        endecUtil = require("EndecUtil"),
        portletContextUtil = require('PortletContextUtil'),
        userUtil = require('UserUtil'),
        logUtil = require('LogUtil'),
        storage = require('storage'),
        endecUtil = require('EndecUtil'),
        resourceLocatorUtil = require('ResourceLocatorUtil'),
        properties = require('Properties'),
        regionsStorage = storage.getCollectionDataStore("regions"),
        timesheetsStorage = storage.getCollectionDataStore('timesheets'),
        schoolsStorage = storage.getCollectionDataStore('schools'),
        appData = require('appData'),
        adminGroup = appData.getNode('admingroup');
    console.log(timesheetsStorage);
    const currentUserId = portletContextUtil.getCurrentUser().getIdentifier();
    const isAdmin = userUtil.isMemberOfGroup(adminGroup);
        
    const monthNames = [
        "jan", "feb", "mars", "april", "maj", "juni",
        "juli", "aug", "sep", "okt", "nov", "dec"
    ];

    return {
        searchTutorsWithWaitingTimesheets: function(tutorName, schoolId) {
            let timesheets = timesheetsStorage.find("ds.analyzed.workState:Väntande", 100000).toArray();
            if (!isAdmin) {
                const regions = regionsStorage.find('ds.analyzed.responsible:' + currentUserId, 1000).toArray();
                timesheets = timesheets.filter(x => regions.some(y => y.dsid === x.region));
            }
            if (schoolId) {
                const escapedSchoolId = schoolId.replace(/[^0-9a-f\-]ig/, "");
                const schools = schoolsStorage.find("ds.analyzed.dsid:" + escapedSchoolId).toArray();
                timesheets = timesheets.filter(x => schools.some(school => school.tutors.some(tutor => tutor.id === x.userid)));
            }
            let tutorsWithTimesheets = _.map(_.groupBy(timesheets, x => x.userid), (value, key) => ({
                id: endecUtil.escapeIdentifier("", key),
                name: properties.get(resourceLocatorUtil.getNodeByIdentifier(key), 'displayName'),
                timesheets: _.map(_.groupBy(value, x => {
                    const date = new Date(x.workDate);
                    return date.getFullYear() + date.getMonth();
                }), (value, key) => {
                    const monthDate = new Date(value[0].workDate);
                    return {
                        month: monthNames[monthDate.getMonth()] + " " + monthDate.getFullYear(),
                        index: monthDate.getMonth(),
                        entries: value,
                        hours: value.reduce((acc, curr) => acc + parseFloat(curr.workHours), 0)
                    };
                })
            }));
            if (tutorName) {
                tutorsWithTimesheets = tutorsWithTimesheets.filter(x => x.name.toLowerCase().includes(tutorName.toLowerCase()));
            }
            tutorsWithTimesheets = tutorsWithTimesheets.sort((a, b) => a.name.localeCompare(b.name));
            return tutorsWithTimesheets;
        // },
        // searchTutorsWithTimesheets: function(tutorName, schoolId) {
        //     let timesheets = timesheetsStorage.find("ds.analyzed.workState:Väntande", 100000).toArray();
        //     if (!isAdmin) {
        //         const regions = regionsStorage.find('ds.analyzed.responsible:' + currentUserId, 1000).toArray();
        //         timesheets = timesheets.filter(x => regions.some(y => y.dsid === x.region));
        //     }
        //     if (schoolId) {
        //         const escapedSchoolId = schoolId.replace(/[^0-9a-f\-]ig/, "");
        //         const schools = schoolsStorage.find("ds.analyzed.dsid:" + escapedSchoolId).toArray();
        //         timesheets = timesheets.filter(x => schools.some(school => school.tutors.some(tutor => tutor.id === x.userid)));
        //     }
        //     let tutorsWithTimesheets = _.map(_.groupBy(timesheets, x => x.userid), (value, key) => ({
        //         id: endecUtil.escapeIdentifier("", key),
        //         name: properties.get(resourceLocatorUtil.getNodeByIdentifier(key), 'displayName'),
        //         timesheets: _.map(_.groupBy(value, x => {
        //             const date = new Date(x.workDate);
        //             return date.getFullYear() + date.getMonth();
        //         }), (value, key) => {
        //             const monthDate = new Date(value[0].workDate);
        //             return {
        //                 month: monthNames[monthDate.getMonth()] + " " + monthDate.getFullYear(),
        //                 index: monthDate.getMonth(),
        //                 entries: value,
        //                 hours: value.reduce((acc, curr) => acc + parseFloat(curr.workHours), 0)
        //             };
        //         })
        //     }));
        //     if (tutorName) {
        //         tutorsWithTimesheets = tutorsWithTimesheets.filter(x => x.name.toLowerCase().includes(tutorName.toLowerCase()));
        //     }
        //     tutorsWithTimesheets = tutorsWithTimesheets.sort((a, b) => a.name.localeCompare(b.name));
        //     return tutorsWithTimesheets;
        }
    };
});