(function () {
   'use strict';

   const
      appData = require('appData'),
      router = require('router'),
      logUtil = require('LogUtil'),
      portletContextUtil = require('PortletContextUtil'),
      userUtil = require('UserUtil'),
      tutorStorageHandler = require('/module/server/tutorStorageHandler'),
      schoolStorageHandler = require("/module/server/schoolStorageHandler"),
      storage = require('storage'),
      tutors = storage.getCollectionDataStore('tutors'),
      timesheets = storage.getCollectionDataStore('timesheets');

   const
      currentUser = portletContextUtil.getCurrentUser(),
      currentUserId = currentUser.getIdentifier(),
      adminGroup = appData.getNode('admingroup'),
      regionGroup = appData.getNode('regiongroup'),
      isAdmin = userUtil.isMemberOfGroup(adminGroup),
      isRegionResponsible = userUtil.isMemberOfGroup(regionGroup);

   router.get('/', function (req, res) {
      if (!isAdmin && !isRegionResponsible) {
         res.send("Du har inte behörighet att se detta material.");
         return;
      }
      
      const query = req.params;
      const tutorName = query.name;
      const schoolId = query.school;
      const tutors = tutorStorageHandler.searchTutorsWithWaitingTimesheets(tutorName, schoolId);
      const schoolAreas = schoolStorageHandler.getAreasWithSchools();
      res.render("/search-timesheets", {
         schoolAreas: schoolAreas,
         tutorName: tutorName,
         schoolId: schoolId,
         tutors: tutors,
         success: false,
         message: "",
         alertType: null
      });
      
      //if (isAdmin || isRegionResponsible) {
      //   var tutorsArray = tutorStorageHandler.fetchTutorsTimesheets(currentUserId, isAdmin, isRegionResponsible);
      //   res.render('/', {
      //      tutors: tutorsArray,
      //      success: false,
      //      message: "",
      //      alertType: null
      //   });
      //} else {
      //   res.send('Du har inte behörighet att se detta material.');
      //}
   });
   /*
   Methods
   */
   router.put('/bulk-timesheets', function (req, res) {
      var dsidArr = req.params.dsid;

      var
         result = null,
         success = false,
         message = "",
         alertType = null;

      for (var i = 0; i < dsidArr.length; i++) {
         try {
            result = timesheets.get(dsidArr[i]);
            if (result) {
               result.workState = "Godkänd";
               try {
                  timesheets.set(dsidArr[i], result);
                  try {
                     timesheets.instantIndex(dsidArr[i]);
                     success = true;
                     alertType = "success";
                  } catch (e) {
                     // Error handling
                     success = true;
                     alertType = "error";
                     logUtil.info('Error in timesheets instantindex - ' + JSON.stringify(e));
                  }
               } catch (e) {
                  success = true;
                  alertType = "error";
                  logUtil.info('Error in timesheets set - ' + JSON.stringify(e));
               }
            }
         } catch (e) {
            // Error handling
            logUtil.info('Error in timesheets get - ' + JSON.stringify(e));
            success = true;
            alertType = "error";
         }
      }
      if (success && alertType !== "error") {
         message += "Tidsrapporterna uppdaterades!";
      } else {
         message += "Ett fel uppstod, kunde inte uppdatera tidsrapporten. Försök igen.";
      }
      res.json({
         tutors: tutorStorageHandler.searchTutorsWithWaitingTimesheets(req.params.searchTutorName, req.params.searchSchoolId),
         success: success,
         message: message,
         alertType: alertType
      });

   });
   router.put('/set-timesheet', function (req, res) {
      var
         dsid = req.params.dsid,
         type = req.params.type,
         textarea = null;

      if (req.params.hasOwnProperty('textarea')) {
         textarea = req.params.textarea;
      }
      var
         result = null,
         success = false,
         message = "",
         alertType = null;

      try {
         result = timesheets.get(dsid);
         if (type === "approved") {
            result.workState = "Godkänd";
         } else if (type === "rejected") {
            result.workState = "Avslagen";
            if (textarea) {
               result.comment = textarea;
            }
         }
         try {
            timesheets.set(dsid, result);
            try {
               timesheets.instantIndex(dsid);
               success = true;
               alertType = "success";
               message += "Tidsrapporten uppdaterades!";
            } catch (e) {
               // Error handling
               logUtil.info('Error in tutors instantindex - ' + JSON.stringify(e));
            }
         } catch (e) {
            logUtil.info('Error in tutors set - ' + JSON.stringify(e));
         }
      } catch (e) {
         // Error handling
         logUtil.info('Error in tutors get - ' + JSON.stringify(e));
         success = true;
         alertType = "error";
         message += "Ett fel uppstod, kunde inte uppdatera tidsrapporten. Försök igen.";
      }
      res.json({
         tutors: tutorStorageHandler.searchTutorsWithWaitingTimesheets(req.params.searchTutorName, req.params.searchSchoolId),
         success: success,
         message: message,
         alertType: alertType
      });
   });
}());