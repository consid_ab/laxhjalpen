define(function (require) {
   'use strict';

   var
      _ = require('underscore'),
      Component = require('Component'),
      $ = require('jquery'),
      router = require('router'),
      requester = require('requester'),
      store = require('store'),
      template = require('/template/main');

   return Component.extend({

      template: template,

      events: {
         dom: {
            'click a.lax-accordion__heading': 'rotateChevron',
            'click button.lax-button__approve--marked': "sendBulkApproval",
            'click button.lax-button__approve': 'sendApproval',
            'click button.lax-button__reject': 'handleModal',
            'click .env-checkbox': 'removeAlert',
            'submit .ros-form--rejectal': 'sendRejectal'
         },
         self: {
            'state:changed': 'render'
         },
         store: 'handleStoreUpdate'
      },
      handleStoreUpdate: function (newState) {
         this.setState(newState);
      },
      sendBulkApproval: function (e) {
         $(e.currentTarget).prop('disabled', true);
         if ($('.lax-timesheet-alert').length) {
            $('.lax-timesheet-alert').remove();
         }
         var
            dsid = $(e.currentTarget).data('dsid'),
            dsidArray = [],
            searchTutorName = $("input[name=name]").val(),
            searchSchoolId = $("select[name=school]").val();

         $(e.currentTarget).closest('.lax-component').find('input[type=checkbox]:checked').each(function (i) {
            dsidArray.push($(this).attr('id'));
         });
         if (dsidArray.length) {
            requester.doPut({
               url: router.getUrl('/bulk-timesheets'),
               data: {
                  dsid: dsidArray,
                  searchTutorName: searchTutorName,
                  searchSchoolId: searchSchoolId
               },
               context: this
            }).done(function (response) {
               store.dispatch({
                  type: 'EDIT_TIMESHEET',
                  tutors: response.tutors,
                  success: response.success,
                  message: response.message,
                  alertType: response.alertType
               });
            }).fail(function (response) {
               console.log(response);
            });
         } else {
            var html = '<div class="lax-timesheet-alert env-alert env-alert--warning" role="alert"><strong> Inga tidsrapporter valda!</strong></div>';
            $(e.currentTarget).after(html);
         }
      },
      sendApproval: function (e) {
         $(e.currentTarget).prop('disabled', true);
         var
            dsid = $(e.currentTarget).data('dsid'),
            type = $(e.currentTarget).data('type'),
            searchTutorName = $("input[name=name]").val(),
            searchSchoolId = $("select[name=school]").val();

         requester.doPut({
            url: router.getUrl('/set-timesheet'),
            data: {
               dsid: dsid,
               type: type,
               searchTutorName: searchTutorName,
               searchSchoolId: searchSchoolId
            },
            context: this
         }).done(function (response) {
            store.dispatch({
               type: 'EDIT_TIMESHEET',
               tutors: response.tutors,
               success: response.success,
               message: response.message,
               alertType: response.alertType
            });
         }).fail(function (response) {
            console.log(response);
         });
      },
      sendRejectal: function (e) {
         e.preventDefault();
         $(e.currentTarget).prop('disabled', true);
         var
            textarea = $(e.currentTarget).find('textarea')[0].value,
            dsid = $(e.currentTarget).find('button[type="submit"]').data('dsid'),
            type = $(e.currentTarget).find('button[type="submit"]').data('type'),
            searchTutorName = $("input[name=name]").val(),
            searchSchoolId = $("select[name=school]").val();

         $('#edit-area-form-' + dsid).envDialog('hide');

         requester.doPut({
            url: router.getUrl('/set-timesheet'),
            data: {
               dsid: dsid,
               textarea: textarea,
               type: type,
               searchTutorName: searchTutorName,
               searchSchoolId: searchSchoolId
            },
            context: this
         }).done(function (response) {
            store.dispatch({
               type: 'EDIT_TIMESHEET',
               tutors: response.tutors,
               success: response.success,
               message: response.message,
               alertType: response.alertType
            });
         }).fail(function (response) {
            console.log(response);
         });
      },
      handleModal: function (e) {
         var dsid = $(e.currentTarget).data('dsid');
         $('#edit-area-form-' + dsid).envDialog('toggle');
      },
      rotateChevron: function (e) {
         if ($('.env-alert').length) {
            $('.env-alert').remove();
         }
         if ($('.fa-chevron-up').length) {
            if ($('.fa-chevron-up')[0] != $(e.currentTarget).find('i.fa-1')[0]) {
               $('.fa-chevron-up').toggleClass('fa-chevron-up fa-chevron-down');
            }
         }
         $(e.currentTarget).find('i.fa-1').toggleClass('fa-chevron-down fa-chevron-up');

      },
      removeAlert: function () {
         if ($('.lax-timesheet-alert').length) {
            $('.lax-timesheet-alert').remove();
         }
      },
      filterState: function (state) {
         return _.extend({}, {
            tutors: state.tutors,
            success: state.success,
            message: state.message,
            alertType: state.alertType
         });
      }
   });
});