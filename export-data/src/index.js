(function () {
   'use strict';

   const router = require('router');
   const appData = require('appData');
   const logUtil = require('LogUtil');
   const dataStorageHandler = require('/module/server/dataStorageHandler');

   router.get('/', (req, res) => {
      res.render('/');
   });
   
   router.get("/get-export-data", (req, res) => {
      const data = dataStorageHandler.getExportData();
      res.set("Content-Type", "application/json");
      res.send(JSON.stringify(data));
   });
})();
