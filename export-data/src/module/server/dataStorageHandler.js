define(function (require) {
    'use strict';

    const
        storage = require('storage'),
        tutorsDataStore = storage.getCollectionDataStore('tutors'),
        schoolsDataStore = storage.getCollectionDataStore('schools'),
        timesheetsDataStore = storage.getCollectionDataStore('timesheets'),
        regionsDataStore = storage.getCollectionDataStore('regions'),
        areasDataStore = storage.getCollectionDataStore('areas'),
        studentsDataStore = storage.getCollectionDataStore('students'),
        groupsDataStore = storage.getCollectionDataStore('groups'),
        properties = require('Properties'),
        resourceLocatorUtil = require('ResourceLocatorUtil');

    return {
        getExportData: function() {
            const maxResults = 100000;
            const tutors = tutorsDataStore.find("*", maxResults).toArray();
            const schools = schoolsDataStore.find("*", maxResults).toArray();
            const timesheets = timesheetsDataStore.find("*", maxResults).toArray();
            const regions = regionsDataStore.find("*", maxResults).toArray();
            const areas = areasDataStore.find("*", maxResults).toArray();
            const students = studentsDataStore.find("*", maxResults).toArray();
            const groups = groupsDataStore.find("*", maxResults).toArray();
            
            const regionsDict = {};
            for (let i = 0; i < regions.length; i++) {
                regionsDict[regions[i].dsid] = regions[i];
            }
            
            const areasDict = {};
            for (let i = 0; i < areas.length; i++) {
                areasDict[areas[i].dsid] = areas[i];
            }
            
            const studentsDict = {};
            for (let i = 0; i < students.length; i++) {
                studentsDict[students[i].dsid] = students[i];
            }
            
            return {
                regions: regions.map(region => ({
                    name: region.name,
                    responsible: properties.get(resourceLocatorUtil.getNodeByIdentifier(region.responsible), 'displayName')
                })),
                areas: areas.map(area => ({
                    name: area.name,
                    region: regionsDict[area.region] ? regionsDict[area.region].name : undefined,
                    coach: properties.get(resourceLocatorUtil.getNodeByIdentifier(area.coach), 'displayName')
                })),
                schools: schools.map(school => ({
                    name: school.name,
                    area: areasDict[school.area] ? areasDict[school.area].name : undefined,
                    address: school.adress,
                    tutors: school.tutors.map(tutor => tutor.name).join(", "),
                    contactPerson: school.contactPerson,
                    contactPersonPhone: school.contactPersonPhone
                })),
                groups: groups.map(group => ({
                    name: group.name,
                    school: group.school,
                    tutors: properties.get(resourceLocatorUtil.getNodeByIdentifier(group.tutors), 'displayName')
                })),
                tutors: tutors.map(tutor => ({
                    name: tutor.name,
                    school: tutor.school,
                    students: (tutor.students || [])
                        .map(studentId => studentsDict[studentId] ? studentsDict[studentId].name : undefined)
                        .filter(x => x !== undefined)
                        .join(", ")
                })),
                students: students.map(student => ({
                    firstName: student.firstname,
                    lastName: student.lastname,
                    phone: student.phone,
                    email: student.email,
                    school: student.school,
                    tutor: student.tutor,
                    grade: student.grade,
                    socialSecurityNumber: student.socialsecurity || "",
                    startDate: student.startdate,
                    endDate: student.enddate,
                    caregiver: student.caregiver,
                    phoneCaregiver: student.phonecaregiver,
                    status: student.status
                })),
                timesheets: timesheets.map(timesheet => ({
                    user: properties.get(resourceLocatorUtil.getNodeByIdentifier(timesheet.userid), 'displayName'),
                    workType: timesheet.workType,
                    workHours: timesheet.workHours,
                    workState: timesheet.workState,
                    workStatus: timesheet.workStatus,
                    workDate: timesheet.workDate,
                    submittedDate: timesheet.sendDate,
                    comment: timesheet.comment
                }))
            };
        }
    };
});