# README

### What is this repository for?

- Repository containing WebApps for Läxhjälpen.
- The WebApps purpose is to work as an operating system for administration but also as an time report system.
- Version 1.0

### How do I get set up?

- Pull repository

### Who do I talk to?

- Repo owner/admin or Consid SiteVision team members.

### Info regarding Data storage

- https://developer.sitevision.se/docs/data-storage

### Info regarding SiteVision WebApps

- https://developer.sitevision.se/docs/webapps

### Info using the WebApp "Students" and a privileged user

- https://developer.sitevision.se/docs/webapps/require/privileged
- The user configured is [webappsystemuser - 4EB99qjgARfCDVmp]
- Activate the user to run as an privileged user - configure the same user to run as privileged on the application.

### Info regarding database model and relations

- Database model
  [DB Model](dbmodel.json)
- Relations
  Regions
  |Areas
  ||Schoolsf
  |||Students
  ||||Groups
  |||||Tutors
  ||||||Timesheets
- Tutors will be added to a School to be available in a group.
- After tutors is added to the groups they will be able to report time.
